-- phpMyAdmin SQL Dump
-- version 4.9.0.1
-- https://www.phpmyadmin.net/
--
-- Host: 127.0.0.1
-- Generation Time: Feb 18, 2020 at 09:22 PM
-- Server version: 10.4.6-MariaDB
-- PHP Version: 7.1.32

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET AUTOCOMMIT = 0;
START TRANSACTION;
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Database: `2020_project_mgmt_rahul`
--

-- --------------------------------------------------------

--
-- Table structure for table `admin`
--

CREATE TABLE `admin` (
  `a_id` int(11) NOT NULL,
  `a_name` varchar(20) NOT NULL,
  `a_email` varchar(20) NOT NULL,
  `a_password` varchar(100) NOT NULL,
  `a_phone` varchar(10) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

--
-- Dumping data for table `admin`
--

INSERT INTO `admin` (`a_id`, `a_name`, `a_email`, `a_password`, `a_phone`) VALUES
(1, 'admin', 'admin@gmail.com', '21232f297a57a5a743894a0e4a801fc3', '9874561230');

-- --------------------------------------------------------

--
-- Table structure for table `coordinator`
--

CREATE TABLE `coordinator` (
  `c_id` int(11) NOT NULL,
  `c_name` varchar(50) NOT NULL,
  `c_email` varchar(50) NOT NULL,
  `c_password` varchar(100) NOT NULL,
  `c_phone` varchar(20) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `coordinator`
--

INSERT INTO `coordinator` (`c_id`, `c_name`, `c_email`, `c_password`, `c_phone`) VALUES
(1, 'Faculty Coordinator', 'coordinator@gmail.com', 'e10adc3949ba59abbe56e057f20f883e', '7412589630');

-- --------------------------------------------------------

--
-- Table structure for table `guide`
--

CREATE TABLE `guide` (
  `g_id` int(11) NOT NULL,
  `g_name` varchar(50) NOT NULL,
  `g_email` varchar(50) NOT NULL,
  `g_password` varchar(100) NOT NULL,
  `g_phone` varchar(20) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `guide`
--

INSERT INTO `guide` (`g_id`, `g_name`, `g_email`, `g_password`, `g_phone`) VALUES
(1, 'Guide 1', 'guide1@gmail.com', 'e10adc3949ba59abbe56e057f20f883e', '7894561230'),
(2, 'Guide 2', 'guide2@gmail.com', 'e10adc3949ba59abbe56e057f20f883e', '7891234560'),
(3, 'Guide 3', 'guide3@gmail.com', 'e10adc3949ba59abbe56e057f20f883e', '7410258963'),
(4, 'Guide 4', 'guide4@gmail.com', 'e10adc3949ba59abbe56e057f20f883e', '6541230789');

-- --------------------------------------------------------

--
-- Table structure for table `hod`
--

CREATE TABLE `hod` (
  `h_id` int(11) NOT NULL,
  `h_name` varchar(50) NOT NULL,
  `h_email` varchar(50) NOT NULL,
  `h_password` varchar(100) NOT NULL,
  `h_phone` varchar(30) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `hod`
--

INSERT INTO `hod` (`h_id`, `h_name`, `h_email`, `h_password`, `h_phone`) VALUES
(1, 'HOD', 'hod@gmail.com', 'e10adc3949ba59abbe56e057f20f883e', '8965741230');

-- --------------------------------------------------------

--
-- Table structure for table `invitation`
--

CREATE TABLE `invitation` (
  `in_id` int(11) NOT NULL,
  `t_id` int(11) NOT NULL,
  `in_from_id` int(11) NOT NULL,
  `in_to_id` int(11) NOT NULL,
  `in_status` int(11) NOT NULL COMMENT '0->Pending, 1->Accepted, 2->Rejected',
  `in_time` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `invitation`
--

INSERT INTO `invitation` (`in_id`, `t_id`, `in_from_id`, `in_to_id`, `in_status`, `in_time`) VALUES
(1, 1, 4, 5, 1, 1581805327),
(2, 1, 4, 6, 1, 1581805372),
(3, 1, 4, 7, 1, 1581805377),
(4, 1, 4, 1, 1, 1581805383),
(5, 1, 5, 1, 1, 1581805580),
(6, 1, 5, 6, 1, 1581805585),
(7, 1, 5, 8, 1, 1581805590),
(8, 1, 5, 9, 1, 1581805603),
(9, 1, 5, 4, 1, 1581805606);

-- --------------------------------------------------------

--
-- Table structure for table `project`
--

CREATE TABLE `project` (
  `p_id` int(11) NOT NULL,
  `p_name` varchar(100) NOT NULL,
  `p_description` text NOT NULL,
  `p_domain` varchar(100) NOT NULL,
  `p_start` varchar(20) NOT NULL,
  `p_end` varchar(20) NOT NULL,
  `g_id` int(11) NOT NULL,
  `t_id` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `project`
--

INSERT INTO `project` (`p_id`, `p_name`, `p_description`, `p_domain`, `p_start`, `p_end`, `g_id`, `t_id`) VALUES
(1, 'Project 1', 'sdfkjsfkljsdfklsdf sfklsdjfklsdf skldfjsklfjsd skldfjsldfjsdf\nsdfjsfjsdf\nsdfjklsldfjlsdf\nsdj;fklsdfjskld\nsdjflsdjsfklj', 'Information Security', '2019-08-01', '2020-03-01', 3, 1),
(2, 'Project 2', 'sdfkjsfkljsdfklsdf sfklsdjfklsdf skldfjsklfjsd skldfjsldfjsdf\nsdfjsfjsdf\nsdfjklsldfjlsdf\nsdj;fklsdfjskld\nsdjflsdjsfklj', 'Information Security', '2019-08-04', '2020-03-06', 1, 0),
(6, 'Quiz App', 'In this project user can play a quiz .After completion of the quiz, user can get result.', 'Android App', '2020-01-01', '2020-03-30', 3, 0),
(7, 'Trader App', 'In this project user can buy a product.', 'Android App', '2020-01-01', '2020-03-31', 3, 0),
(8, 'Medical App', 'In this project user can book appointments and buy medicine', 'Android App', '2020-01-15', '2020-03-11', 4, 0);

-- --------------------------------------------------------

--
-- Table structure for table `project_creator`
--

CREATE TABLE `project_creator` (
  `pc_id` int(11) NOT NULL,
  `pc_name` varchar(50) NOT NULL,
  `pc_email` varchar(50) NOT NULL,
  `pc_password` varchar(100) NOT NULL,
  `pc_phone` varchar(30) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `project_creator`
--

INSERT INTO `project_creator` (`pc_id`, `pc_name`, `pc_email`, `pc_password`, `pc_phone`) VALUES
(1, 'Project Creator', 'creator@gmail.com', 'e10adc3949ba59abbe56e057f20f883e', '4567891230');

-- --------------------------------------------------------

--
-- Table structure for table `project_mentor`
--

CREATE TABLE `project_mentor` (
  `pm_id` int(11) NOT NULL,
  `pm_name` varchar(50) NOT NULL,
  `pm_email` varchar(50) NOT NULL,
  `pm_password` varchar(100) NOT NULL,
  `pm_phone` varchar(30) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `project_mentor`
--

INSERT INTO `project_mentor` (`pm_id`, `pm_name`, `pm_email`, `pm_password`, `pm_phone`) VALUES
(1, 'Project Mentor', 'mentor@gmail.com', 'e10adc3949ba59abbe56e057f20f883e', '7894561320');

-- --------------------------------------------------------

--
-- Table structure for table `project_task`
--

CREATE TABLE `project_task` (
  `pt_id` int(11) NOT NULL,
  `p_id` int(11) NOT NULL,
  `pt_title` varchar(200) NOT NULL,
  `pt_status` int(11) NOT NULL COMMENT '0->incomplete, 1->Complete'
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `project_task`
--

INSERT INTO `project_task` (`pt_id`, `p_id`, `pt_title`, `pt_status`) VALUES
(1, 1, 'Task One', 0),
(2, 1, 'Task two', 0),
(3, 1, 'Task three', 0),
(4, 1, 'Task four', 0),
(5, 2, 'Task five', 0),
(6, 2, 'Task six', 0),
(7, 6, 'Quiz App', 0);

-- --------------------------------------------------------

--
-- Table structure for table `project_task_comment`
--

CREATE TABLE `project_task_comment` (
  `ptc_id` int(11) NOT NULL,
  `pt_id` int(11) NOT NULL,
  `pm_id` int(11) NOT NULL,
  `g_id` int(11) NOT NULL,
  `s_id` int(11) NOT NULL,
  `ptc_message` text NOT NULL,
  `ptc_time` varchar(20) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `project_task_comment`
--

INSERT INTO `project_task_comment` (`ptc_id`, `pt_id`, `pm_id`, `g_id`, `s_id`, `ptc_message`, `ptc_time`) VALUES
(1, 1, 1, 0, 0, 'Complete task as early as possible', '1581969410'),
(2, 1, 0, 1, 0, 'No update on task since last few days', '1581969433'),
(3, 1, 0, 0, 1, 'We are working on the project. We are facing some problem in task', '1581969488'),
(4, 1, 0, 1, 0, 'Please elaborate on problem', '1581969509'),
(5, 1, 0, 0, 1, 'Task completed. Kindly check task at http://www.google.com', '1581969533'),
(6, 2, 0, 1, 0, 'When will you start this task?', '1581969567'),
(7, 2, 1, 0, 0, 'I want to review this task fast.', '1581969584'),
(8, 2, 0, 0, 1, 'Task completed.', '1581969599');

-- --------------------------------------------------------

--
-- Table structure for table `student`
--

CREATE TABLE `student` (
  `s_id` int(11) NOT NULL,
  `s_name` varchar(20) NOT NULL,
  `s_email` varchar(20) NOT NULL,
  `s_password` varchar(100) NOT NULL,
  `s_phone` varchar(20) NOT NULL,
  `s_c_id` varchar(20) NOT NULL,
  `s_department` varchar(50) NOT NULL,
  `s_year` varchar(10) NOT NULL,
  `t_id` int(11) NOT NULL COMMENT '0->Not selected, else team ID',
  `s_time` varchar(20) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

--
-- Dumping data for table `student`
--

INSERT INTO `student` (`s_id`, `s_name`, `s_email`, `s_password`, `s_phone`, `s_c_id`, `s_department`, `s_year`, `t_id`, `s_time`) VALUES
(1, 'sonu', 'sonu@gmail.com', 'e10adc3949ba59abbe56e057f20f883e', '9874561230', '101', 'Information Technolo', '2019', 0, ''),
(2, 'nikita', 'nikita@gmail.com', 'e10adc3949ba59abbe56e057f20f883e', '9632580147', '102', 'Computer Science', '2019', 3, ''),
(3, 'sony', 'aony@gmail.com', 'e10adc3949ba59abbe56e057f20f883e', '9632580147', '109', 'Information technolo', '2019', 0, ''),
(4, 'Student 1', 'student1@gmail.com', 'e10adc3949ba59abbe56e057f20f883e', '9632587410', '1234567981', 'IT', '2020', 1, '1581802587'),
(5, 'Student 2', 'student2@gmail.com', 'e10adc3949ba59abbe56e057f20f883e', '7894561590', '45595623652', 'IT', '2020', 2, '1581802607'),
(6, 'Student 3', 'student3@gmail.com', 'e10adc3949ba59abbe56e057f20f883e', '741541963', '753695120', 'IT', '2020', 1, '1581802626'),
(7, 'Student 4', 'student4@gmail.com', 'e10adc3949ba59abbe56e057f20f883e', '1236545230', '7536951756', 'IT', '2020', 0, '1581802770'),
(8, 'Student 5', 'student5@gmail.com', 'e10adc3949ba59abbe56e057f20f883e', '8521456320', '7537456962', 'IT', '2020', 0, '1581802791'),
(9, 'Student 6', 'student6@gmail.com', 'e10adc3949ba59abbe56e057f20f883e', '7459856230', '7597456962', 'IT', '2020', 0, '1581802810'),
(10, 'Student 7', 'student7@gmail.com', 'e10adc3949ba59abbe56e057f20f883e', '4568521520', '7414569652', 'IT', '2020', 0, '1581802931'),
(13, '', '', 'd41d8cd98f00b204e9800998ecf8427e', '', '', '', '', 0, '1581918380'),
(17, 'Nikita Satam', 'nikita12@gmail.com', 'e10adc3949ba59abbe56e057f20f883e', '9632580147', '101', 'Information technology', '2020', 0, '1581919574');

-- --------------------------------------------------------

--
-- Table structure for table `team`
--

CREATE TABLE `team` (
  `t_id` int(11) NOT NULL,
  `t_created` int(11) NOT NULL COMMENT 's_id who create team',
  `t_name` varchar(100) NOT NULL,
  `t_topic` varchar(200) NOT NULL,
  `t_time` varchar(20) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `team`
--

INSERT INTO `team` (`t_id`, `t_created`, `t_name`, `t_topic`, `t_time`) VALUES
(1, 4, '404 Not Found', '', '1581803943'),
(2, 5, 'ArtDroid', '', '1581803970'),
(3, 2, 'rahul', '', '1582033116');

--
-- Indexes for dumped tables
--

--
-- Indexes for table `admin`
--
ALTER TABLE `admin`
  ADD PRIMARY KEY (`a_id`);

--
-- Indexes for table `coordinator`
--
ALTER TABLE `coordinator`
  ADD PRIMARY KEY (`c_id`);

--
-- Indexes for table `guide`
--
ALTER TABLE `guide`
  ADD PRIMARY KEY (`g_id`),
  ADD UNIQUE KEY `g_email` (`g_email`);

--
-- Indexes for table `hod`
--
ALTER TABLE `hod`
  ADD PRIMARY KEY (`h_id`);

--
-- Indexes for table `invitation`
--
ALTER TABLE `invitation`
  ADD PRIMARY KEY (`in_id`);

--
-- Indexes for table `project`
--
ALTER TABLE `project`
  ADD PRIMARY KEY (`p_id`);

--
-- Indexes for table `project_creator`
--
ALTER TABLE `project_creator`
  ADD PRIMARY KEY (`pc_id`);

--
-- Indexes for table `project_mentor`
--
ALTER TABLE `project_mentor`
  ADD PRIMARY KEY (`pm_id`);

--
-- Indexes for table `project_task`
--
ALTER TABLE `project_task`
  ADD PRIMARY KEY (`pt_id`);

--
-- Indexes for table `project_task_comment`
--
ALTER TABLE `project_task_comment`
  ADD PRIMARY KEY (`ptc_id`);

--
-- Indexes for table `student`
--
ALTER TABLE `student`
  ADD PRIMARY KEY (`s_id`),
  ADD UNIQUE KEY `s_email` (`s_email`);

--
-- Indexes for table `team`
--
ALTER TABLE `team`
  ADD PRIMARY KEY (`t_id`);

--
-- AUTO_INCREMENT for dumped tables
--

--
-- AUTO_INCREMENT for table `admin`
--
ALTER TABLE `admin`
  MODIFY `a_id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=2;

--
-- AUTO_INCREMENT for table `coordinator`
--
ALTER TABLE `coordinator`
  MODIFY `c_id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=2;

--
-- AUTO_INCREMENT for table `guide`
--
ALTER TABLE `guide`
  MODIFY `g_id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=5;

--
-- AUTO_INCREMENT for table `hod`
--
ALTER TABLE `hod`
  MODIFY `h_id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=2;

--
-- AUTO_INCREMENT for table `invitation`
--
ALTER TABLE `invitation`
  MODIFY `in_id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=10;

--
-- AUTO_INCREMENT for table `project`
--
ALTER TABLE `project`
  MODIFY `p_id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=9;

--
-- AUTO_INCREMENT for table `project_creator`
--
ALTER TABLE `project_creator`
  MODIFY `pc_id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=2;

--
-- AUTO_INCREMENT for table `project_mentor`
--
ALTER TABLE `project_mentor`
  MODIFY `pm_id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=2;

--
-- AUTO_INCREMENT for table `project_task`
--
ALTER TABLE `project_task`
  MODIFY `pt_id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=8;

--
-- AUTO_INCREMENT for table `project_task_comment`
--
ALTER TABLE `project_task_comment`
  MODIFY `ptc_id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=9;

--
-- AUTO_INCREMENT for table `student`
--
ALTER TABLE `student`
  MODIFY `s_id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=18;

--
-- AUTO_INCREMENT for table `team`
--
ALTER TABLE `team`
  MODIFY `t_id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=4;
COMMIT;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
