<?php

	include dirname(dirname(__FILE__)).DIRECTORY_SEPARATOR.'config.php';

	$response = array();

	$s_id = $_POST['s_id'];

	$sql = "SELECT * FROM `student` WHERE t_id = 0 AND s_id <> ".$s_id;
	$result = mysqli_query($con, $sql);

	if(mysqli_num_rows($result)>0){
		$data = array();

		while($row = mysqli_fetch_assoc($result))
			array_push($data, $row);

		$response['success'] = '1';
		$response['message'] = 'Student list having not selected team';
		$response['data'] = $data;
	}else{
		$response['success'] = '0';
		$response['message'] = 'All students selected team.';
	}

	echo json_encode($response);

?>