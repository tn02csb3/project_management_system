<?php
	include dirname(dirname(__FILE__)).DIRECTORY_SEPARATOR.'config.php';

	$response = array();

	$sql = "SELECT project.*, guide.g_name, team.t_name FROM `project` LEFT JOIN guide ON guide.g_id = project.g_id LEFT JOIN team ON project.t_id = team.t_id WHERE project.t_id = 0";
	$result = mysqli_query($con, $sql);

	if(mysqli_num_rows($result)>0){
		$data = array();
		while($row = mysqli_fetch_assoc($result)){
			if($row['g_name'] == null)
				$row['g_name'] = "Not Assigned";

			if($row['t_name'] == null)
				$row['t_name'] = "Not Selected";
			array_push($data, $row);
		}

		$response['success'] = '1';
		$response['message'] = 'Projects available in system.';
		$response['data'] = $data;
	}else{
		$response['success'] = '0';
		$response['message'] = 'No project available in system.';
	}

	echo json_encode($response);


?>