<?php

	include dirname(dirname(__FILE__)).DIRECTORY_SEPARATOR.'config.php';

	$response = array();


	$sql = "SELECT * FROM `student`";
	$result = mysqli_query($con, $sql);

	if(mysqli_num_rows($result)>0){
		$data = array();

		while($row = mysqli_fetch_assoc($result))
			array_push($data, $row);

		$response['success'] = '1';
		$response['message'] = 'Student Data Available';
		$response['data'] = $data;
	}else{
		$response['success'] = '0';
		$response['message'] = 'No Data';
	}

	echo json_encode($response);

?>