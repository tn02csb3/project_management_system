<?php
	include dirname(dirname(__FILE__)).DIRECTORY_SEPARATOR.'config.php';

	$response = array();

	if(isset($_POST['p_name']) && isset($_POST['p_description']) && isset($_POST['p_domain']) && isset($_POST['p_start']) && isset($_POST['p_end'])){
		$p_name = $_POST['p_name'];
		$p_description = mysqli_real_escape_string($con, $_POST['p_description']);
		$p_domain = $_POST['p_domain'];
		$p_start = $_POST['p_start'];
		$p_end = $_POST['p_end'];

		$sql = "INSERT INTO `project`(`p_name`, `p_description`, `p_domain`, `p_start`, `p_end`) VALUES ('".$p_name."', '".$p_description."', '".$p_domain."', '".$p_start."', '".$p_end."')";
		$result = mysqli_query($con, $sql);

		if($result != null){
			$response['success'] = '1';
			$response['message'] = 'Project added successfully.';
		}else{
			$response['success'] = '0';
			$response['message'] = 'Technical problem arises. Please try again later.';
		}
	}else{
		$response['success'] = '0';
		$response['message'] = 'Parameter missing.';
	}

	echo json_encode($response);


?>