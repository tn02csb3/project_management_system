<?php

	include dirname(dirname(__FILE__)).DIRECTORY_SEPARATOR.'config.php';

	$response = array();

	$pt_id = $_POST['pt_id'];

	$sql = "SELECT project_task_comment.*, project_mentor.pm_name, guide.g_name, coordinator.c_name, student.s_name FROM `project_task_comment` LEFT JOIN project_mentor ON project_mentor.pm_id = project_task_comment.pm_id LEFT JOIN guide ON guide.g_id = project_task_comment.g_id LEFT JOIN coordinator ON coordinator.c_id = project_task_comment.c_id LEFT JOIN student ON student.s_id = project_task_comment.s_id WHERE pt_id = ".$pt_id;
	$result = mysqli_query($con, $sql);

	if(mysqli_num_rows($result)>0){
		$data = array();
		while($row = mysqli_fetch_assoc($result)){
			$row['ptc_time'] = date('Y-m-d H:i', $row['ptc_time']);

			if($row['pm_name'] == null)
				$row['pm_name'] = '';
			if($row['g_name'] == null)
				$row['g_name'] = '';
			if($row['c_name'] == null)
				$row['c_name'] = '';
			if($row['s_name'] == null)
				$row['s_name'] = '';

			array_push($data, $row);
		}
		$response['success'] = '1';
		$response['message'] = 'Tasks comments available for the project task.';
		$response['data'] = $data;
	}else{
		$response['success'] = '0';
		$response['message'] = 'No task comment available for project task.';
	}

	echo json_encode($response);


?>