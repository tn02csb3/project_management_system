package com.college.guideadapter;

import android.content.Context;
import android.content.Intent;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.ProgressBar;
import android.widget.TextView;
import android.widget.Toast;

import androidx.annotation.NonNull;
import androidx.cardview.widget.CardView;
import androidx.recyclerview.widget.RecyclerView;

import com.android.volley.AuthFailureError;
import com.android.volley.Request;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.StringRequest;
import com.college.admin.AdminMenuActivity;
import com.college.guide.GuideAddCommentActivity;
import com.college.hod.HodLoginActivity;
import com.college.pojo.Task;
import com.college.student.R;
import com.college.util.AppController;
import com.college.util.Keys;
import com.college.util.Loggers;
import com.college.util.SharedPreference;

import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.Map;

public class GuideTaskStatusAdapter extends RecyclerView.Adapter<GuideTaskStatusAdapter.ViewHolder> {

    private Context context;
    private ArrayList<Task> list;

    public GuideTaskStatusAdapter(Context context, ArrayList<Task> list) {
        this.context = context;
        this.list = list;
    }

    @NonNull
    @Override
    public ViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        LayoutInflater layoutInflater = LayoutInflater.from(context);
        View listItem = layoutInflater.inflate(R.layout.changestatus, parent, false);
        listItem.setLayoutParams(new RecyclerView.LayoutParams(RecyclerView.LayoutParams.MATCH_PARENT, RecyclerView.LayoutParams.WRAP_CONTENT));
        return new GuideTaskStatusAdapter.ViewHolder(listItem);


    }

    @Override
    public void onBindViewHolder(@NonNull final ViewHolder holder, int position) {
        final Task task=list.get(position);
        holder.tv_title.setText("Title: "+task.getPt_title());
        holder.tv_status.setText("Status: "+task.getPt_status());

        if (task.getPt_status().equals("0")){
            holder.tv_status.setText("Status: " + "Incomplete");
            holder.button_approve.setVisibility(View.VISIBLE);
        }else if(task.getPt_status().equals("1")){
            holder.tv_status.setText("Status: " + "Complete");
        }

        holder.cd.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent=new Intent(context, GuideAddCommentActivity.class);
                intent.addFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
                intent.putExtra("pt_id",task.getPt_id());
                intent.putExtra("p_id",task.getP_id());
                context.startActivity(intent);
            }
        });
        holder.button_approve.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                holder.progressBar.setVisibility(View.VISIBLE);
                StringRequest request=new StringRequest(Request.Method.POST, Keys.URL.CHANGE_STATUS, new Response.Listener<String>() {
                    @Override
                    public void onResponse(String response) {
                        Loggers.i(response);
                        holder.progressBar.setVisibility(View.GONE);
                        try {
                            JSONObject jsonObject=new JSONObject(response);
                            if (jsonObject.getString("success").equals("1")){
                                Toast.makeText(context, jsonObject.getString("message"), Toast.LENGTH_SHORT).show();
                                holder.tv_status.setText("Status: Complete");
                                holder.button_approve.setVisibility(View.GONE);


                            }else {
                                Toast.makeText(context, jsonObject.getString("message"), Toast.LENGTH_SHORT).show();
                            }


                        } catch (JSONException e) {
                            e.printStackTrace();
                            Toast.makeText(context, "Technical problem arises", Toast.LENGTH_SHORT).show();
                        }

                    }
                }, new Response.ErrorListener() {
                    @Override
                    public void onErrorResponse(VolleyError error) {
                        holder.progressBar.setVisibility(View.GONE);
                        error.printStackTrace();
                    }
                }){
                    @Override
                    protected Map<String, String> getParams() throws AuthFailureError {
                        Map<String,String> params=new HashMap<>();
                        params.put("pt_id",task.getPt_id());
                        params.put("pt_status","2");
                        return params;
                    }
                };

                AppController.getInstance().add(request);


            }
        });




    }

    @Override
    public int getItemCount() {

        return list.size();
    }

    public class ViewHolder extends RecyclerView.ViewHolder {

        TextView tv_title,tv_status;
        CardView cd;
        ImageView imageView;
        Button button_approve;
        ProgressBar progressBar;


        public ViewHolder(@NonNull View itemView) {
            super(itemView);
            this.tv_title=itemView.findViewById(R.id.txt_title);
            this.tv_status=itemView.findViewById(R.id.txt_status);
            this.cd=itemView.findViewById(R.id.card_view);
            button_approve=itemView.findViewById(R.id.btn_approve);
            progressBar=itemView.findViewById(R.id.progress_status);





        }
    }
}
