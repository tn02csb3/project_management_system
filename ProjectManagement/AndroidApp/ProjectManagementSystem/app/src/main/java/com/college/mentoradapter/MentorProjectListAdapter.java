package com.college.mentoradapter;

import android.content.Context;
import android.content.Intent;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.cardview.widget.CardView;
import androidx.recyclerview.widget.RecyclerView;

import com.college.mentor.MentorProjectTaskListActivity;
import com.college.pojo.Project;
import com.college.student.R;

import java.util.ArrayList;

public class MentorProjectListAdapter extends RecyclerView.Adapter<MentorProjectListAdapter.ViewHolder> {

    private Context context;
    private ArrayList<Project> list;

    public MentorProjectListAdapter(Context context, ArrayList<Project> list) {
        this.context = context;
        this.list = list;
    }

    @NonNull
    @Override
    public MentorProjectListAdapter.ViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        LayoutInflater layoutInflater = LayoutInflater.from(context);
        View listItem = layoutInflater.inflate(R.layout.projectlist, parent, false);
        listItem.setLayoutParams(new RecyclerView.LayoutParams(RecyclerView.LayoutParams.MATCH_PARENT, RecyclerView.LayoutParams.WRAP_CONTENT));

        return new MentorProjectListAdapter.ViewHolder(listItem);
    }

    @Override
    public void onBindViewHolder(@NonNull MentorProjectListAdapter.ViewHolder holder, int position) {
        final Project project=list.get(position);
        holder.tv_name.setText(project.getP_name());
        holder.tv_desc.setText(project.getP_description());
        holder.tv_domain.setText("Domain: "+project.getP_domain());
        holder.tv_start.setText(project.getP_start());
        holder.tv_end.setText(project.getP_end());
        holder.cd.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent=new Intent(context, MentorProjectTaskListActivity.class);
                intent.addFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
                intent.putExtra("p_id",project.getP_id());
                context.startActivity(intent);
            }
        });



    }

    @Override
    public int getItemCount() {
        return list.size();
    }

    @Override
    public long getItemId(int position) {
       return  position;
    }

    public class ViewHolder extends RecyclerView.ViewHolder {
        TextView tv_name,tv_desc,tv_domain,tv_start,tv_end;
        CardView cd;

        public ViewHolder(@NonNull View itemView) {
            super(itemView);
            this.tv_name=itemView.findViewById(R.id.txt_p_name);
            this.tv_desc=itemView.findViewById(R.id.txt_p_desc);
            this.tv_domain=itemView.findViewById(R.id.txt_p_domain);
            this.tv_start=itemView.findViewById(R.id.txt_p_start);
            this.tv_end=itemView.findViewById(R.id.txt_p_end);
            this.cd=itemView.findViewById(R.id.project_card);



        }
    }
}
