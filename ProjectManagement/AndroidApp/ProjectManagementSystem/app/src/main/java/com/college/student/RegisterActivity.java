package com.college.student;

import androidx.appcompat.app.AppCompatActivity;

import android.content.Intent;
import android.os.Bundle;
import android.util.Patterns;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ProgressBar;
import android.widget.Toast;

import com.android.volley.AuthFailureError;
import com.android.volley.Request;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.StringRequest;
import com.college.util.AppController;
import com.college.util.Keys;

import org.json.JSONException;
import org.json.JSONObject;

import java.util.HashMap;
import java.util.Map;

public class RegisterActivity extends AppCompatActivity {

    Button button_register;
    EditText editText_name,editText_email,editText_pass,editText_c_pass,editText_phone,editText_college_id,editText_departement,editText_year;
    ProgressBar pBarReg;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_register);
        button_register=findViewById(R.id.btn_u_register);
        editText_name=findViewById(R.id.edt_name);
        editText_email=findViewById(R.id.edt_email);
        editText_pass=findViewById(R.id.edt_pass);
        editText_c_pass=findViewById(R.id.edt_c_pass);
        editText_phone=findViewById(R.id.edt_phone);
        editText_college_id=findViewById(R.id.edt_college_id);
        editText_departement=findViewById(R.id.edt_department);
        editText_year=findViewById(R.id.edt_year);
        button_register=findViewById(R.id.btn_u_register);
        pBarReg = findViewById(R.id.progressBarRegistration);
        button_register.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
               String s_name=editText_name.getText().toString().trim();
               String s_email=editText_email.getText().toString().trim();
               String s_password=editText_pass.getText().toString().trim();
               String s_conf_pass=editText_c_pass.getText().toString().trim();
               String s_phone=editText_phone.getText().toString().trim();
               String s_c_id=editText_college_id.getText().toString().trim();
               String s_department=editText_departement.getText().toString().trim();
               String s_year=editText_year.getText().toString().trim();
               if (s_name.equals("") || s_email.equals("") || s_password.equals("") || s_conf_pass.equals("") || s_phone.equals("")
               || s_c_id.equals("") || s_department.equals("") || s_year.equals("")){
                   Toast.makeText(RegisterActivity.this, "please fill all the details", Toast.LENGTH_SHORT).show();
               }else if (!Patterns.EMAIL_ADDRESS.matcher(s_email).matches()){
                   editText_email.setError("Invalid Email Id");
               }else if (!s_conf_pass.equals(s_password)){
                   editText_c_pass.setError("please confirm the password");
               }else if (s_phone.length()!=10){
                   editText_phone.setError("phone number must be 10 digit");
               }else{
                   register(s_name,s_email,s_password,s_phone,s_c_id,s_department,s_year);
               }

            }
        });
    }

    private void register(final String s_name, final String s_email, final String s_password, final String s_phone, final String s_c_id, final String s_department, final String s_year) {
        pBarReg.setVisibility(View.VISIBLE);
        StringRequest request=new StringRequest(Request.Method.POST, Keys.URL.STUDENT_REGISTER, new Response.Listener<String>() {
            @Override
            public void onResponse(String response) {
                pBarReg.setVisibility(View.GONE);
                try {
                    JSONObject jsonObject=new JSONObject(response);
                    if (jsonObject.getString("success").equals("1")){

                        Toast.makeText(RegisterActivity.this, jsonObject.getString("message"), Toast.LENGTH_SHORT).show();
                        Intent intent=new Intent(RegisterActivity.this,LoginActivity.class);
                        intent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
                        startActivity(intent);
                        clear();



                    }else {
                        Toast.makeText(RegisterActivity.this, jsonObject.getString("message"), Toast.LENGTH_SHORT).show();
                    }


                } catch (JSONException e) {
                    e.printStackTrace();

                }

            }
        }, new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError error) {
                pBarReg.setVisibility(View.GONE);
                error.printStackTrace();
                Toast.makeText(RegisterActivity.this, "Technical problem arises", Toast.LENGTH_SHORT).show();

            }
        }){
            @Override
            protected Map<String, String> getParams() throws AuthFailureError {
                Map<String,String> params=new HashMap<>();
                params.put("s_name",s_name);
                params.put("s_email",s_email);
                params.put("s_password",s_password);
                params.put("s_phone",s_phone);
                params.put("s_c_id",s_c_id);
                params.put("s_department",s_department);
                params.put("s_year",s_year);
                return params;
            }
        };
        AppController.getInstance().add(request);



    }

//    private void register(final String u_name, final String u_email, final String u_password, final String u_phone, final String c_id, final String u_department, final String u_year) {
//        pBarReg.setVisibility(View.VISIBLE);
//        StringRequest request=new StringRequest(Request.Method.POST, Keys.URL.USER_REGISTER, new Response.Listener<String>() {
//            @Override
//            public void onResponse(String response) {
//                pBarReg.setVisibility(View.GONE);
//                try {
//                    JSONObject jsonObject=new JSONObject(response);
//                    if (jsonObject.getString("success").equals("1")){
//
//                        Toast.makeText(RegisterActivity.this, jsonObject.getString("message"), Toast.LENGTH_SHORT).show();
//                        clear();
//
//
//                    }else {
//                        Toast.makeText(RegisterActivity.this, jsonObject.getString("message"), Toast.LENGTH_SHORT).show();
//                    }
//
//
//                } catch (JSONException e) {
//                    e.printStackTrace();
//
//                }
//
//            }
//        }, new Response.ErrorListener() {
//            @Override
//            public void onErrorResponse(VolleyError error) {
//                pBarReg.setVisibility(View.GONE);
//                error.printStackTrace();
//                Toast.makeText(RegisterActivity.this, "Technical problem arises", Toast.LENGTH_SHORT).show();
//
//            }
//        }){
//            @Override
//            protected Map<String, String> getParams() throws AuthFailureError {
//                Map<String,String> params=new HashMap<>();
//                params.put("u_name",u_name);
//                params.put("u_email",u_email);
//                params.put("u_password",u_password);
//                params.put("u_phone",u_phone);
//                params.put("c_id",c_id);
//                params.put("u_department",u_department);
//                params.put("u_year",u_year);
//                return params;
//            }
//        };
//        AppController.getInstance().add(request);
//
//
//
//    }

    private void clear() {
        editText_name.setText("");
        editText_email.setText("");
        editText_pass.setText("");
        editText_c_pass.setText("");
        editText_phone.setText("");
        editText_college_id.setText("");
        editText_departement.setText("");
        editText_year.setText("");

    }
}
