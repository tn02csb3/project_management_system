package com.college.projectcreator;

import androidx.annotation.NonNull;
import androidx.appcompat.app.AppCompatActivity;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import android.content.Intent;
import android.os.Bundle;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.view.View;
import android.widget.ProgressBar;

import com.android.volley.AuthFailureError;
import com.android.volley.Request;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.StringRequest;
import com.college.creatoradapter.CreatorProjectListAdapter;
import com.college.pojo.CommonProjectList;
import com.college.student.MenuActivity;
import com.college.student.R;
import com.college.util.AppController;
import com.college.util.Keys;
import com.college.util.Loggers;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.Map;

public class CreatorProjectListActivity extends AppCompatActivity {

    RecyclerView recyclerView;
    ArrayList<CommonProjectList> list;
    ProgressBar progressBar;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_creator_project_list);
        progressBar=findViewById(R.id.progressbar_list);
        recyclerView=findViewById(R.id.recycler_common_project);
        list=new ArrayList<>();
        getSupportActionBar().setTitle("Project List");
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        LinearLayoutManager linearLayoutManager=new LinearLayoutManager(getApplicationContext());
        recyclerView.setLayoutManager(linearLayoutManager);
        recyclerView.setHasFixedSize(true);

    }

    @Override
    protected void onStart() {
        super.onStart();
        projectList();
    }

    private void projectList() {
        list.clear();
        progressBar.setVisibility(View.VISIBLE);
        StringRequest request=new StringRequest(Request.Method.POST, Keys.URL.SEE_ALL_PROJECT, new Response.Listener<String>() {
            @Override
            public void onResponse(String response) {
                Loggers.i(response);
                progressBar.setVisibility(View.GONE);
                try {
                    JSONObject jsonObject=new JSONObject(response);
                    if (jsonObject.getString("success").equals("1")){
                        JSONArray jsonArray=jsonObject.getJSONArray("data");

                        for (int i=0;i<jsonArray.length();i++){
                            JSONObject jsonObject1=jsonArray.getJSONObject(i);
                            list.add(new CommonProjectList(jsonObject1.getString("p_id"),
                                    jsonObject1.getString("p_name"),
                                    jsonObject1.getString("p_description"),
                                    jsonObject1.getString("p_domain"),
                                    jsonObject1.getString("p_start"),
                                    jsonObject1.getString("p_end"),
                                    jsonObject1.getString("g_id"),
                                    jsonObject1.getString("t_id")));


                        }
                        CreatorProjectListAdapter adapter=new CreatorProjectListAdapter(getApplicationContext(),list);
                        recyclerView.setAdapter(adapter);


                    }else {

                    }


                } catch (JSONException e) {
                    e.printStackTrace();
                }


            }
        }, new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError error) {
                progressBar.setVisibility(View.GONE);
                error.printStackTrace();
            }
        }){
            @Override
            protected Map<String, String> getParams() throws AuthFailureError {
                Map<String,String> params=new HashMap<>();
                return params;
            }
        };

        AppController.getInstance().add(request);

    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {

            MenuInflater inflater = getMenuInflater();
            inflater.inflate(R.menu.add_project, menu);
            return true;
    }


    @Override
    public boolean onOptionsItemSelected(@NonNull MenuItem item) {
        switch (item.getItemId()){
            case android.R.id.home:
                onBackPressed();
                break;
            case R.id.add_project:
                Intent intent=new Intent(CreatorProjectListActivity.this, CreatorAddProjectActivity.class);
                startActivity(intent);
                break;

        }
        return super.onOptionsItemSelected(item);


    }


    @Override
    public void onBackPressed() {
        Intent intent=new Intent(CreatorProjectListActivity.this, MenuActivity.class);
        intent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
        startActivity(intent);
        finish();
    }
}
