package com.college.projectcreator;

import androidx.annotation.NonNull;
import androidx.appcompat.app.AppCompatActivity;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import android.content.Intent;
import android.os.Bundle;
import android.util.Log;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.view.View;
import android.widget.ProgressBar;

import com.android.volley.AuthFailureError;
import com.android.volley.Request;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.StringRequest;
import com.college.creatoradapter.CreatorTaskListAdapter;
import com.college.pojo.Task;
import com.college.student.R;
import com.college.util.AppController;
import com.college.util.Keys;
import com.college.util.Loggers;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.Map;

public class CreatorProjectTaskActivity extends AppCompatActivity {


    RecyclerView recyclerView;
    String p_id;
    ProgressBar progressBar;
    ArrayList<Task> list;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_creator_project_task);
        getSupportActionBar().setTitle("Task List");
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        recyclerView=findViewById(R.id.recycler_task_list);
        progressBar=findViewById(R.id.progress_task);
        list=new ArrayList<>();
        LinearLayoutManager linearLayoutManager=new LinearLayoutManager(getApplicationContext());
        recyclerView.setLayoutManager(linearLayoutManager);
        recyclerView.setHasFixedSize(true);
        Intent i=getIntent();
        p_id=i.getStringExtra("p_id");
        Log.i("p_id",String.valueOf(p_id));



    }


    @Override
    protected void onStart() {
        super.onStart();
        taskList(p_id);
    }

    private void taskList(final String p_id) {
        list.clear();
        progressBar.setVisibility(View.VISIBLE);
        StringRequest request=new StringRequest(Request.Method.POST, Keys.URL.TASK_LIST, new Response.Listener<String>() {
            @Override
            public void onResponse(String response) {
                Loggers.i(response);
                progressBar.setVisibility(View.GONE);
                try {
                    JSONObject jsonObject=new JSONObject(response);
                    if (jsonObject.getString("success").equals("1")){
                        JSONArray jsonArray=jsonObject.getJSONArray("data");

                        for (int i=0;i<jsonArray.length();i++){
                            JSONObject jsonObject1=jsonArray.getJSONObject(i);
                            list.add(new Task(jsonObject1.getString("pt_id"),
                                    jsonObject1.getString("p_id"),
                                    jsonObject1.getString("pt_title"),
                                    jsonObject1.getString("pt_status")));


                        }
                        CreatorTaskListAdapter adapter=new CreatorTaskListAdapter(getApplicationContext(),list);
                        recyclerView.setAdapter(adapter);


                    }else {

                    }


                } catch (JSONException e) {
                    e.printStackTrace();
                }


            }
        }, new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError error) {
                progressBar.setVisibility(View.GONE);
                error.printStackTrace();
            }
        }){
            @Override
            protected Map<String, String> getParams() throws AuthFailureError {
                Map<String,String> params=new HashMap<>();
                params.put("p_id",p_id);
                return params;
            }
        };

        AppController.getInstance().add(request);
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        MenuInflater inflater = getMenuInflater();
        inflater.inflate(R.menu.add_project, menu);
        return true;


    }

    @Override
    public boolean onOptionsItemSelected(@NonNull MenuItem item) {
        switch (item.getItemId()){

            case android.R.id.home:
                onBackPressed();
                break;
            case R.id.add_project:
                Intent intent=new Intent(CreatorProjectTaskActivity.this, CreatorAddTaskActivity.class);
                intent.putExtra("p_id",p_id);
                startActivity(intent);
                break;
        }
        return super.onOptionsItemSelected(item);


    }
    @Override
    public void onBackPressed() {
        Intent intent=new Intent(CreatorProjectTaskActivity.this, CreatorProjectListActivity.class);
        intent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
        startActivity(intent);
        finish();
    }
}
