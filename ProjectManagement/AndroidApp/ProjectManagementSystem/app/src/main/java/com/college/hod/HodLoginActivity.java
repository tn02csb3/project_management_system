package com.college.hod;

import androidx.appcompat.app.AppCompatActivity;

import android.content.Intent;
import android.os.Bundle;
import android.util.Patterns;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ProgressBar;
import android.widget.Toast;

import com.android.volley.AuthFailureError;
import com.android.volley.Request;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.StringRequest;
import com.college.student.MenuActivity;
import com.college.student.R;
import com.college.util.AppController;
import com.college.util.Keys;
import com.college.util.SharedPreference;

import org.json.JSONException;
import org.json.JSONObject;

import java.util.HashMap;
import java.util.Map;

public class HodLoginActivity extends AppCompatActivity {


    Button button_login;
    EditText editText_email,editText_pass;
    ProgressBar progressBar;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_hod_login);
        editText_email=findViewById(R.id.edt_h_email);
        editText_pass=findViewById(R.id.edt_h_pass);
        button_login=findViewById(R.id.btn_h_login);
        progressBar=findViewById(R.id.progressBar_h_login);
        getSupportActionBar().setTitle("Login");
        button_login.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                String h_email=editText_email.getText().toString().trim();
                String h_password=editText_pass.getText().toString().trim();
                if (h_email.equals("") || h_password.equals("")){
                    Toast.makeText(HodLoginActivity.this, "please fill all the details", Toast.LENGTH_SHORT).show();
                }else if (!Patterns.EMAIL_ADDRESS.matcher(h_email).matches()){
                    editText_email.setError("Invalid Email ID");
                }else {
                    login(h_email,h_password);
                }
            }
        });


    }

    private void login(final String h_email, final String h_password) {
        progressBar.setVisibility(View.VISIBLE);
        StringRequest request=new StringRequest(Request.Method.POST, Keys.URL.HOD_LOGIN, new Response.Listener<String>() {
            @Override
            public void onResponse(String response) {
                progressBar.setVisibility(View.GONE);
                try {
                    JSONObject jsonObject=new JSONObject(response);
                    if (jsonObject.getString("success").equals("1")){

                        Toast.makeText(HodLoginActivity.this, jsonObject.getString("message"), Toast.LENGTH_SHORT).show();
                        Intent intent=new Intent(HodLoginActivity.this, HODProjectActivity.class);
                        intent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
                        SharedPreference.save("h_email",h_email);
                        SharedPreference.save("h_password",h_password);
                        startActivity(intent);
                        clear();
                        finish();

                    }else {
                        Toast.makeText(HodLoginActivity.this, jsonObject.getString("message"), Toast.LENGTH_SHORT).show();
                    }


                } catch (JSONException e) {
                    e.printStackTrace();
                    Toast.makeText(HodLoginActivity.this, "Technical problem arises", Toast.LENGTH_SHORT).show();
                }

            }
        }, new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError error) {
                progressBar.setVisibility(View.GONE);
                error.printStackTrace();
            }
        }){
            @Override
            protected Map<String, String> getParams() throws AuthFailureError {
                Map<String,String> params=new HashMap<>();
                params.put("h_email",h_email);
                params.put("h_password",h_password);
                return params;
            }
        };

        AppController.getInstance().add(request);





    }

    private void clear() {

        editText_email.setText("");
        editText_pass.setText("");
    }


    @Override
    public void onBackPressed() {
        Intent intent=new Intent(HodLoginActivity.this, MenuActivity.class);
        intent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
        startActivity(intent);
        finish();


    }
}
