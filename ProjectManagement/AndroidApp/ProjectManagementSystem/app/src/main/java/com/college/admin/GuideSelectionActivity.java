package com.college.admin;

import androidx.annotation.NonNull;
import androidx.appcompat.app.AppCompatActivity;

import android.content.Intent;
import android.os.Bundle;
import android.util.Log;
import android.view.MenuItem;
import android.view.View;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.Spinner;
import android.widget.TextView;
import android.widget.Toast;

import com.android.volley.AuthFailureError;
import com.android.volley.Request;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.StringRequest;
import com.college.student.R;
import com.college.util.AppController;
import com.college.util.Keys;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.Map;

public class GuideSelectionActivity extends AppCompatActivity {

    Spinner spinner;
    ArrayList<String> list;
    ArrayList<String> list_g_id;
    Button button_submit;
    String g_name;
    int g_id;
    TextView tv_title;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_guide_selection);
        getSupportActionBar().setTitle("Guide Selection");
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        tv_title = findViewById(R.id.txt_p_title);
        Intent i = getIntent();
        final String p_id = i.getStringExtra("p_id");
        String p_name = i.getStringExtra("p_name");
        tv_title.setText(p_name);
        Log.i("nik", p_id);

        spinner = findViewById(R.id.spinner_list);
        list = new ArrayList<>();
        list_g_id = new ArrayList<>();
        button_submit = findViewById(R.id.btn_submit);
        guidelist();
        /*StringRequest request = new StringRequest(Request.Method.POST, Keys.URL.SEE_ALL_GUIDES, new Response.Listener<String>() {
            @Override
            public void onResponse(String response) {
                try {
                    JSONObject jsonObject = new JSONObject(response);
                    if (jsonObject.getString("success").equals("1")) {
                        JSONArray jsonArray = jsonObject.getJSONArray("data");

                        for (int i = 0; i < jsonArray.length(); i++) {
                            JSONObject jsonObject1 = jsonArray.getJSONObject(i);
                            g_id = jsonObject1.getInt("g_id");
                            Loggers.i(String.valueOf(g_id));
                            //newList.add(jsonObject1.getString("g_id"));


                        }


                    } else {

                    }


                } catch (JSONException e) {
                    e.printStackTrace();
                }

            }
        }, new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError error) {
                error.printStackTrace();
            }
        }) {
            @Override
            protected Map<String, String> getParams() throws AuthFailureError {
                return super.getParams();
            }
        };
        AppController.getInstance().add(request);*/
        button_submit.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                g_name = spinner.getSelectedItem().toString();
                g_id = spinner.getSelectedItemPosition();

                if (g_name.equals("")) {
                    Toast.makeText(GuideSelectionActivity.this, "please select guide", Toast.LENGTH_SHORT).show();
                } else {

                    assignGuide(p_id, list_g_id.get(spinner.getSelectedItemPosition()));
                }

            }
        });

        //storeData();


    }


    private void guidelist() {
        StringRequest request = new StringRequest(Request.Method.POST, Keys.URL.SEE_ALL_GUIDES, new Response.Listener<String>() {
            @Override
            public void onResponse(String response) {

                try {
                    JSONObject jsonObject = new JSONObject(response);
                    if (jsonObject.getString("success").equals("1")) {
                        JSONArray jsonArray = jsonObject.getJSONArray("data");

                        for (int i = 0; i < jsonArray.length(); i++) {
                            JSONObject jsonObject1 = jsonArray.getJSONObject(i);
                            list.add(jsonObject1.getString("g_name"));
                            list_g_id.add(jsonObject1.getString("g_id"));


                        }
                        ArrayAdapter adapter = new ArrayAdapter(GuideSelectionActivity.this, android.R.layout.simple_spinner_dropdown_item, list);
                        spinner.setAdapter(adapter);


                    } else {

                    }


                } catch (JSONException e) {
                    e.printStackTrace();
                }


            }
        }, new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError error) {
                error.printStackTrace();
            }
        }) {
            @Override
            protected Map<String, String> getParams() throws AuthFailureError {
                Map<String, String> params = new HashMap<>();
                return params;
            }
        };

        AppController.getInstance().add(request);


    }

    private void assignGuide(final String p_id, final String g_id) {
        StringRequest request = new StringRequest(Request.Method.POST, Keys.URL.ASSIGN_PROJECT_GUIDE, new Response.Listener<String>() {
            @Override
            public void onResponse(String response) {
                try {
                    JSONObject jsonObject = new JSONObject(response);
                    if (jsonObject.getString("success").equals("1")) {

                        Toast.makeText(GuideSelectionActivity.this, jsonObject.getString("message"), Toast.LENGTH_SHORT).show();

                    } else {
                        Toast.makeText(GuideSelectionActivity.this, jsonObject.getString("message"), Toast.LENGTH_SHORT).show();
                    }


                } catch (JSONException e) {
                    e.printStackTrace();
                    Toast.makeText(GuideSelectionActivity.this, "Technical problem arises", Toast.LENGTH_SHORT).show();
                }
            }
        }, new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError error) {
                error.printStackTrace();
            }
        }) {
            @Override
            protected Map<String, String> getParams() throws AuthFailureError {
                Map<String, String> params = new HashMap<>();
                params.put("p_id", p_id);
                params.put("g_id", g_id);
                return params;
            }
        };
        AppController.getInstance().add(request);
    }


    @Override
    public boolean onOptionsItemSelected(@NonNull MenuItem item) {
        switch (item.getItemId()) {
            case android.R.id.home:
                onBackPressed();
                break;
        }

        return super.onOptionsItemSelected(item);
    }
}
