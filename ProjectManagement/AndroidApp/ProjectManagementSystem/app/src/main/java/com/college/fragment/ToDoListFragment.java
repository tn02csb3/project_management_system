package com.college.fragment;

import android.content.Intent;
import android.os.Bundle;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ProgressBar;
import android.widget.Toast;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.fragment.app.Fragment;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import com.android.volley.AuthFailureError;
import com.android.volley.Request;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.StringRequest;
import com.college.pojo.Task;
import com.college.student.R;
import com.college.studentadapter.ProjectTaskAdapter;
import com.college.util.AppController;
import com.college.util.Keys;
import com.college.util.Loggers;
import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.Map;

public class ToDoListFragment extends Fragment {

    RecyclerView recyclerView;
    String p_id;
    ProgressBar progressBar;
    ArrayList<Task> list;

    public View onCreateView(@NonNull LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.fragment_tasklist, container, false);
        recyclerView=view.findViewById(R.id.recycler_task_list);
        progressBar=view.findViewById(R.id.progress_task);
        list=new ArrayList<>();
        LinearLayoutManager linearLayoutManager=new LinearLayoutManager(getContext());
        recyclerView.setLayoutManager(linearLayoutManager);
        recyclerView.setHasFixedSize(true);
        Intent i=getActivity().getIntent();
        p_id=i.getStringExtra("p_id");
        Log.i("p_id",String.valueOf(p_id));
        return view;


    }

    @Override
    public void onStart() {
        super.onStart();
        taskList(p_id);
    }

    private void taskList(final String p_id) {
        list.clear();
        progressBar.setVisibility(View.VISIBLE);
        StringRequest request=new StringRequest(Request.Method.POST, Keys.URL.TASK_LIST, new Response.Listener<String>() {
            @Override
            public void onResponse(String response) {
                Loggers.i(response);
                progressBar.setVisibility(View.GONE);
                try {
                    JSONObject jsonObject=new JSONObject(response);
                    if (jsonObject.getString("success").equals("1")){
                        JSONArray jsonArray=jsonObject.getJSONArray("data");

                        for (int i=0;i<jsonArray.length();i++){
                            JSONObject jsonObject1=jsonArray.getJSONObject(i);
                            list.add(new Task(jsonObject1.getString("pt_id"),
                                    jsonObject1.getString("p_id"),
                                    jsonObject1.getString("pt_title"),
                                    jsonObject1.getString("pt_status")));


                        }
                        ProjectTaskAdapter adapter=new ProjectTaskAdapter(getContext(),list);
                        recyclerView.setAdapter(adapter);


                    }else {
                        Toast.makeText(getContext(), "No Task Available", Toast.LENGTH_SHORT).show();
                    }


                } catch (JSONException e) {
                    e.printStackTrace();
                }


            }
        }, new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError error) {
                progressBar.setVisibility(View.GONE);
                error.printStackTrace();
            }
        }){
            @Override
            protected Map<String, String> getParams() throws AuthFailureError {
                Map<String,String> params=new HashMap<>();
                params.put("p_id",p_id);
                return params;
            }
        };

        AppController.getInstance().add(request);
    }





//    @Override
//    public boolean onCreateOptionsMenu(Menu menu) {
//
//        MenuInflater inflater = getActivity().getMenuInflater();
//        inflater.inflate(R.menu.team, menu);
//        return true;
//
//
//
//    }
//
//    @Override
//    public boolean onOptionsItemSelected(@NonNull MenuItem item) {
//        switch (item.getItemId()){
//
//            case R.id.team_list:
//                Intent intent=new Intent(ProjectTaskActivity.this, TeamListActivity.class);
//                intent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
//                startActivity(intent);
//        }
//        return super.onOptionsItemSelected(item);
//
//    }


}
