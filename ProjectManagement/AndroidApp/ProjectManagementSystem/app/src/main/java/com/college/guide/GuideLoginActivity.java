package com.college.guide;

import androidx.appcompat.app.AppCompatActivity;

import android.content.Intent;
import android.os.Bundle;
import android.util.Patterns;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ProgressBar;
import android.widget.Toast;

import com.android.volley.AuthFailureError;
import com.android.volley.Request;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.StringRequest;
import com.college.student.MenuActivity;
import com.college.student.R;
import com.college.util.AppController;
import com.college.util.Keys;
import com.college.util.SharedPreference;

import org.json.JSONException;
import org.json.JSONObject;

import java.util.HashMap;
import java.util.Map;

public class GuideLoginActivity extends AppCompatActivity {



    Button button_login;
    EditText editText_email,editText_pass;
    ProgressBar progressBar;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_guide_login);
        editText_email=findViewById(R.id.edt_g_email);
        editText_pass=findViewById(R.id.edt_g_pass);
        button_login=findViewById(R.id.btn_g_login);
        progressBar=findViewById(R.id.progressBar_g_login);
        getSupportActionBar().setTitle("Login");
        button_login.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                String g_email=editText_email.getText().toString().trim();
                String g_password=editText_pass.getText().toString().trim();
                if (g_email.equals("") || g_password.equals("")){
                    Toast.makeText(GuideLoginActivity.this, "please fill all the details", Toast.LENGTH_SHORT).show();
                }else if (!Patterns.EMAIL_ADDRESS.matcher(g_email).matches()){
                    editText_email.setError("Invalid Email ID");
                }else {
                    login(g_email,g_password);
                }
            }
        });


    }

    private void login(final String g_email, final String g_password) {
        progressBar.setVisibility(View.VISIBLE);
        StringRequest request=new StringRequest(Request.Method.POST, Keys.URL.GUIDE_LOGIN, new Response.Listener<String>() {
            @Override
            public void onResponse(String response) {
                progressBar.setVisibility(View.GONE);
                try {
                    JSONObject jsonObject=new JSONObject(response);
                    if (jsonObject.getString("success").equals("1")){
                        JSONObject jsonObject1=jsonObject.getJSONObject("data");
                        String g_id=jsonObject1.getString("g_id");
                        Toast.makeText(GuideLoginActivity.this, jsonObject.getString("message"), Toast.LENGTH_SHORT).show();
                        Intent intent=new Intent(GuideLoginActivity.this, GuideProjectListActivity.class);
                        SharedPreference.save("g_id",g_id);
                        SharedPreference.save("g_email",g_email);
                        SharedPreference.save("g_password",g_password);
                        startActivity(intent);
                        clear();
                        finish();

                    }else {
                        Toast.makeText(GuideLoginActivity.this, jsonObject.getString("message"), Toast.LENGTH_SHORT).show();
                    }


                } catch (JSONException e) {
                    e.printStackTrace();
                    Toast.makeText(GuideLoginActivity.this, "Technical problem arises", Toast.LENGTH_SHORT).show();
                }

            }
        }, new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError error) {
                progressBar.setVisibility(View.GONE);
                error.printStackTrace();
            }
        }){
            @Override
            protected Map<String, String> getParams() throws AuthFailureError {
                Map<String,String> params=new HashMap<>();
                params.put("g_email",g_email);
                params.put("g_password",g_password);
                return params;
            }
        };
        AppController.getInstance().add(request);




    }

    private void clear() {

        editText_email.setText("");
        editText_pass.setText("");
    }


    @Override
    public void onBackPressed() {
        Intent intent=new Intent(GuideLoginActivity.this, MenuActivity.class);
        intent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
        startActivity(intent);
        finish();
    }
}
