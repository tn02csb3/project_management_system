package com.college.studentadapter;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.cardview.widget.CardView;
import androidx.recyclerview.widget.RecyclerView;

import com.college.pojo.ProjectList;
import com.college.student.R;

import java.util.ArrayList;

public class AvailableProjectListAdapter extends RecyclerView.Adapter<AvailableProjectListAdapter.ViewHolder> {

    private Context context;
    private ArrayList<ProjectList> list;

    public AvailableProjectListAdapter(Context context, ArrayList<ProjectList> list) {
        this.context = context;
        this.list = list;
    }

    @NonNull
    @Override
    public ViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        LayoutInflater layoutInflater = LayoutInflater.from(context);
        View listItem = layoutInflater.inflate(R.layout.availableprojectlist, parent, false);
        listItem.setLayoutParams(new RecyclerView.LayoutParams(RecyclerView.LayoutParams.MATCH_PARENT, RecyclerView.LayoutParams.WRAP_CONTENT));
        return new AvailableProjectListAdapter.ViewHolder(listItem);


    }

    @Override
    public void onBindViewHolder(@NonNull ViewHolder holder, int position) {
        final ProjectList projectList=list.get(position);
        holder.tv_name.setText(projectList.getP_name());
        holder.tv_desc.setText(projectList.getP_description());
        holder.tv_domain.setText("Domain: "+projectList.getP_domain());
        holder.tv_start.setText(projectList.getP_start());
        holder.tv_end.setText(projectList.getP_end());
        holder.tv_guide.setText(projectList.getG_name());
//        holder.tv_team.setText(projectList.getT_name());



    }

    @Override
    public int getItemCount() {

        return list.size();
    }

    public class ViewHolder extends RecyclerView.ViewHolder {

        TextView tv_name,tv_desc,tv_domain,tv_start,tv_end,tv_guide,tv_team;
        CardView cd;



        public ViewHolder(@NonNull View itemView) {
            super(itemView);
            this.tv_name=itemView.findViewById(R.id.txt_p_name);
            this.tv_desc=itemView.findViewById(R.id.txt_p_desc);
            this.tv_domain=itemView.findViewById(R.id.txt_p_domain);
            this.tv_start=itemView.findViewById(R.id.txt_p_start);
            this.tv_end=itemView.findViewById(R.id.txt_p_end);
            this.cd=itemView.findViewById(R.id.project_card);
            this.tv_guide=itemView.findViewById(R.id.txt_guide_name);






        }
    }
}
