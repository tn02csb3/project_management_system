package com.college.projectcreator;

import androidx.annotation.NonNull;
import androidx.appcompat.app.AppCompatActivity;

import android.content.Intent;
import android.os.Bundle;
import android.util.Log;
import android.view.MenuItem;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ProgressBar;
import android.widget.Toast;

import com.android.volley.AuthFailureError;
import com.android.volley.Request;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.StringRequest;
import com.college.mentor.AddProjectTaskActivity;
import com.college.student.R;
import com.college.util.AppController;
import com.college.util.Keys;

import org.json.JSONException;
import org.json.JSONObject;

import java.util.HashMap;
import java.util.Map;

public class CreatorAddTaskActivity extends AppCompatActivity {

    EditText editText_title;
    Button button_add_task;
    ProgressBar progressBar;
    String p_id;



    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_creator_add_task);

        editText_title=findViewById(R.id.edt_p_title);
        button_add_task=findViewById(R.id.btn_addTask);
        progressBar=findViewById(R.id.progressBar_add_task);
        getSupportActionBar().setTitle("Add Task");
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        progressBar=findViewById(R.id.progressBar_add_task);
        Intent i=getIntent();
        p_id=i.getStringExtra("p_id");
        Log.i("p_id",p_id);


        button_add_task.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                String pt_title=editText_title.getText().toString().trim();
                if (pt_title.equals("")){
                    Toast.makeText(CreatorAddTaskActivity.this, "Please fill all the details", Toast.LENGTH_SHORT).show();
                }else {
                    addTask(p_id,pt_title);
                }
            }
        });
    }

    private void addTask(final String p_id, final String pt_title) {
        progressBar.setVisibility(View.VISIBLE);
        StringRequest request=new StringRequest(Request.Method.POST, Keys.URL.ADD_TASK, new Response.Listener<String>() {
            @Override
            public void onResponse(String response) {
                progressBar.setVisibility(View.GONE);
                try {
                    JSONObject jsonObject=new JSONObject(response);
                    if (jsonObject.getString("success").equals("1")){

                        Toast.makeText(CreatorAddTaskActivity.this, jsonObject.getString("message"), Toast.LENGTH_SHORT).show();
                        clear();
                        finish();

                    }else {
                        Toast.makeText(CreatorAddTaskActivity.this, jsonObject.getString("message"), Toast.LENGTH_SHORT).show();
                    }


                } catch (JSONException e) {
                    e.printStackTrace();
                    Toast.makeText(CreatorAddTaskActivity.this, "Technical problem arises", Toast.LENGTH_SHORT).show();
                }

            }
        }, new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError error) {
                progressBar.setVisibility(View.GONE);
                error.printStackTrace();

            }
        })
        {
            @Override
            protected Map<String, String> getParams() throws AuthFailureError {
                Map<String,String> params=new HashMap<>();
                params.put("p_id",p_id);
                params.put("pt_title",pt_title);
                return params;
            }
        };
        AppController.getInstance().add(request);

    }

    private void clear() {
        editText_title.setText("");

    }




    @Override
    public boolean onOptionsItemSelected(@NonNull MenuItem item) {
        switch (item.getItemId()){
            case android.R.id.home:
                onBackPressed();
                break;
        }

        return super.onOptionsItemSelected(item);

    }
}
