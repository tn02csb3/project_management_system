package com.college.student;

import androidx.appcompat.app.AppCompatActivity;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import android.widget.ProgressBar;
import android.widget.Toast;

import com.android.volley.AuthFailureError;
import com.android.volley.Request;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.StringRequest;
import com.college.studentadapter.AvailableProjectListAdapter;
import com.college.pojo.ProjectList;
import com.college.pojo.RecyclerItemClickListener;
import com.college.util.AppController;
import com.college.util.Keys;
import com.college.util.Loggers;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.Map;

public class AvailableProjectActivity extends AppCompatActivity {

    RecyclerView recyclerView;
    ArrayList<ProjectList> list;
    ProgressBar progressBar;
    String t_id;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_available_project);
        progressBar=findViewById(R.id.progressbar_list);
        recyclerView=findViewById(R.id.recycler_available_project);
        list=new ArrayList<>();
        Intent i=getIntent();
        t_id=i.getStringExtra("t_id");
        getSupportActionBar().setTitle("Project List");
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        LinearLayoutManager linearLayoutManager=new LinearLayoutManager(getApplicationContext());
        recyclerView.setLayoutManager(linearLayoutManager);
        recyclerView.setHasFixedSize(true);
        recyclerView.addOnItemTouchListener(new RecyclerItemClickListener(getApplicationContext(), recyclerView, new RecyclerItemClickListener.OnItemClickListener() {
            @Override
            public void onItemClick(View view, int position) {
                String p_id = list.get(position).getP_id();
                selectProject(t_id,p_id);
            }

            @Override
            public void onLongItemClick(View view, int position) {

            }
        }));



    }

    private void selectProject(final String t_id, final String p_id) {

        list.clear();
        progressBar.setVisibility(View.VISIBLE);
        StringRequest request=new StringRequest(Request.Method.POST, Keys.URL.SELECT_PROJECT, new Response.Listener<String>() {
            @Override
            public void onResponse(String response) {
                Loggers.i(response);
                progressBar.setVisibility(View.GONE);
                try {
                    JSONObject jsonObject=new JSONObject(response);
                    if (jsonObject.getString("success").equals("1")){
                        Toast.makeText(AvailableProjectActivity.this, jsonObject.getString("message"), Toast.LENGTH_SHORT).show();
                    }else {
                        Toast.makeText(AvailableProjectActivity.this, jsonObject.getString("message"), Toast.LENGTH_SHORT).show();
                    }


                } catch (JSONException e) {
                    e.printStackTrace();
                }


            }
        }, new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError error) {
                progressBar.setVisibility(View.GONE);
                error.printStackTrace();
            }
        }){
            @Override
            protected Map<String, String> getParams() throws AuthFailureError {
                Map<String,String> params=new HashMap<>();
                params.put("p_id",p_id);
                params.put("t_id",t_id);
                return params;
            }
        };

        AppController.getInstance().add(request);



    }


    @Override
    protected void onStart() {
        super.onStart();
        projectList();
    }

    private void projectList() {
        list.clear();
        progressBar.setVisibility(View.VISIBLE);
        StringRequest request=new StringRequest(Request.Method.POST, Keys.URL.AVAILABLE_PROJECTS, new Response.Listener<String>() {
            @Override
            public void onResponse(String response) {
                Loggers.i(response);
                progressBar.setVisibility(View.GONE);
                try {
                    JSONObject jsonObject=new JSONObject(response);
                    if (jsonObject.getString("success").equals("1")){
                        JSONArray jsonArray=jsonObject.getJSONArray("data");

                        for (int i=0;i<jsonArray.length();i++){
                            JSONObject jsonObject1=jsonArray.getJSONObject(i);
                            list.add(new ProjectList(jsonObject1.getString("p_id"),
                                    jsonObject1.getString("p_name"),
                                    jsonObject1.getString("p_description"),
                                    jsonObject1.getString("p_domain"),
                                    jsonObject1.getString("p_start"),
                                    jsonObject1.getString("p_end"),
                                    jsonObject1.getString("g_id"),
                                    jsonObject1.getString("t_id"),
                                    jsonObject1.getString("g_name"),
                                    jsonObject1.getString("t_name")));


                        }
                        AvailableProjectListAdapter adapter=new AvailableProjectListAdapter(getApplicationContext(),list);
                        recyclerView.setAdapter(adapter);


                    }else {

                    }


                } catch (JSONException e) {
                    e.printStackTrace();
                }


            }
        }, new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError error) {
                progressBar.setVisibility(View.GONE);
                error.printStackTrace();
            }
        }){
            @Override
            protected Map<String, String> getParams() throws AuthFailureError {
                Map<String,String> params=new HashMap<>();
                return params;
            }
        };

        AppController.getInstance().add(request);



    }

}
