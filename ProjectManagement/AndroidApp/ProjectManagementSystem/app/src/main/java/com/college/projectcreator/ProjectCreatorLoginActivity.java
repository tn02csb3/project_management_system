package com.college.projectcreator;

import androidx.appcompat.app.AppCompatActivity;

import android.content.Intent;
import android.os.Bundle;
import android.util.Patterns;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ProgressBar;
import android.widget.Toast;

import com.android.volley.AuthFailureError;
import com.android.volley.Request;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.StringRequest;
import com.college.student.MenuActivity;
import com.college.student.R;
import com.college.util.AppController;
import com.college.util.Keys;
import com.college.util.SharedPreference;

import org.json.JSONException;
import org.json.JSONObject;

import java.util.HashMap;
import java.util.Map;

public class ProjectCreatorLoginActivity extends AppCompatActivity {


    Button button_login;
    EditText editText_email,editText_pass;
    ProgressBar progressBar;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_project_creator_login);
        getSupportActionBar().setTitle("Login");
        editText_email=findViewById(R.id.edt_p_email);
        editText_pass=findViewById(R.id.edt_p_pass);
        button_login=findViewById(R.id.btn_p_login);
        progressBar=findViewById(R.id.progressBar_p_login);
        button_login.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                String pc_email=editText_email.getText().toString().trim();
                String pc_password=editText_pass.getText().toString().trim();
                if (pc_email.equals("") || pc_password.equals("")){
                    Toast.makeText(ProjectCreatorLoginActivity.this, "please fill all the details", Toast.LENGTH_SHORT).show();
                }else if (!Patterns.EMAIL_ADDRESS.matcher(pc_email).matches()){
                    editText_email.setError("Invalid Email ID");
                }else {
                    login(pc_email,pc_password);
                }
            }
        });


    }

    private void login(final String pc_email, final String pc_password) {
        progressBar.setVisibility(View.VISIBLE);
        StringRequest request=new StringRequest(Request.Method.POST, Keys.URL.CREATOR_LOGIN, new Response.Listener<String>() {
            @Override
            public void onResponse(String response) {
                progressBar.setVisibility(View.GONE);
                try {
                    JSONObject jsonObject=new JSONObject(response);
                    if (jsonObject.getString("success").equals("1")){

                        JSONObject jsonObject1=jsonObject.getJSONObject("data");
                        String pc_id=jsonObject1.getString("pc_id");

                        Toast.makeText(ProjectCreatorLoginActivity.this, jsonObject.getString("message"), Toast.LENGTH_SHORT).show();
                        Intent intent=new Intent(ProjectCreatorLoginActivity.this, CreatorProjectListActivity.class);
                        SharedPreference.save("pc_id",pc_id);
                        SharedPreference.save("pc_email",pc_email);
                        SharedPreference.save("pc_password",pc_password);
                        startActivity(intent);
                        clear();
                        finish();

                    }else {
                        Toast.makeText(ProjectCreatorLoginActivity.this, jsonObject.getString("message"), Toast.LENGTH_SHORT).show();
                    }


                } catch (JSONException e) {
                    e.printStackTrace();
                    Toast.makeText(ProjectCreatorLoginActivity.this, "Technical problem arises", Toast.LENGTH_SHORT).show();
                }

            }
        }, new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError error) {
                progressBar.setVisibility(View.GONE);
                error.printStackTrace();
            }
        }){
            @Override
            protected Map<String, String> getParams() throws AuthFailureError {
                Map<String,String> params=new HashMap<>();
                params.put("pc_email",pc_email);
                params.put("pc_password",pc_password);
                return params;
            }
        };

        AppController.getInstance().add(request);





    }

    private void clear() {

        editText_email.setText("");
        editText_pass.setText("");
    }


    @Override
    public void onBackPressed() {
        Intent intent=new Intent(ProjectCreatorLoginActivity.this, MenuActivity.class);
        intent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
        startActivity(intent);
        finish();
    }
}
