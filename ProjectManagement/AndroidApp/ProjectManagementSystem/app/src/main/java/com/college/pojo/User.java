package com.college.pojo;

public class User {

    private String name;
    private String email;
    private String phone;
    private String id;
    private String dept;
    private String year;



    public User(String name, String email, String phone, String id, String dept, String year) {
        this.name = name;
        this.email = email;
        this.phone = phone;
        this.id = id;
        this.dept = dept;
        this.year = year;
    }


    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getEmail() {
        return email;
    }

    public void setEmail(String email) {
        this.email = email;
    }

    public String getPhone() {
        return phone;
    }

    public void setPhone(String phone) {
        this.phone = phone;
    }

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public String getDept() {
        return dept;
    }

    public void setDept(String dept) {
        this.dept = dept;
    }

    public String getYear() {
        return year;
    }

    public void setYear(String year) {
        this.year = year;
    }


}
