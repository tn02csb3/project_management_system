package com.college.adminadapter;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.cardview.widget.CardView;
import androidx.recyclerview.widget.RecyclerView;

import com.college.pojo.Student;
import com.college.student.R;

import java.util.ArrayList;

public class UserAdapter extends RecyclerView.Adapter<UserAdapter.ViewHolder> {

    private Context context;
    private ArrayList<Student> list;

    public UserAdapter(Context context, ArrayList<Student> list) {
        this.context = context;
        this.list = list;
    }

    @NonNull
    @Override
    public UserAdapter.ViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        LayoutInflater layoutInflater = LayoutInflater.from(context);
        View listItem = layoutInflater.inflate(R.layout.userlist, parent, false);
        listItem.setLayoutParams(new RecyclerView.LayoutParams(RecyclerView.LayoutParams.MATCH_PARENT, RecyclerView.LayoutParams.WRAP_CONTENT));

        return new UserAdapter.ViewHolder(listItem);
    }

    @Override
    public void onBindViewHolder(@NonNull UserAdapter.ViewHolder holder, int position) {
        Student student = list.get(position);
        holder.tv_name.setText("Name: " + student.getS_name());
        holder.tv_email.setText("Email: " +student.getS_email());
        holder.tv_phone.setText("Phone: " + student.getS_phone());
        holder.tv_id.setText("College ID: " + student.getS_c_id());
        holder.tv_dept.setText("Department: " + student.getS_department());
        holder.tv_year.setText("Year: " + student.getS_year());


        if (student.getSelected_year().equals(student.getS_year())){
            holder.cd.setVisibility(View.VISIBLE);
        }else{
            holder.cd.setVisibility(View.GONE);
        }

        if (student.getSelected_year().equals(student.getS_year())){
            holder.cd.setVisibility(View.VISIBLE);
        }else{
            holder.cd.setVisibility(View.GONE);
        }
        if (student.getSelected_year().equals(student.getS_year())){
            holder.cd.setVisibility(View.VISIBLE);
        }else{
            holder.cd.setVisibility(View.GONE);
        }




    }

    @Override
    public int getItemCount() {
        return list.size();
    }

    public class ViewHolder extends RecyclerView.ViewHolder {

        TextView tv_name, tv_email, tv_phone, tv_id, tv_dept, tv_year;
        Button button_send_invitation;
        CardView cd;


        public ViewHolder(@NonNull View itemView) {
            super(itemView);
            this.tv_name = itemView.findViewById(R.id.txt_name);
            this.tv_email = itemView.findViewById(R.id.txt_email);
            this.tv_phone = itemView.findViewById(R.id.txt_phone);
            this.tv_id = itemView.findViewById(R.id.txt_college_id);
            this.tv_dept = itemView.findViewById(R.id.txt_department);
            this.tv_year = itemView.findViewById(R.id.txt_year);
            cd=itemView.findViewById(R.id.card_view);


        }
    }
}
