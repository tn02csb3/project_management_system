package com.college.hod;

import androidx.annotation.NonNull;
import androidx.appcompat.app.AppCompatActivity;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import android.content.Intent;
import android.os.Bundle;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.view.View;
import android.widget.ProgressBar;
import android.widget.Toast;

import com.android.volley.AuthFailureError;
import com.android.volley.Request;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.StringRequest;
import com.college.commonadapter.CommentListAdapter;
import com.college.guide.ChangeStatusActivity;
import com.college.guide.GuideAddCommentActivity;
import com.college.guide.GuideLoginActivity;
import com.college.pojo.Comment;
import com.college.student.MenuActivity;
import com.college.student.R;
import com.college.util.AppController;
import com.college.util.Keys;
import com.college.util.Loggers;
import com.college.util.SharedPreference;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.Map;

public class ShowCommentsActivity extends AppCompatActivity {

    ProgressBar progressBar;
    RecyclerView recyclerView;
    ArrayList<Comment> list;
    String pt_id,p_id;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_show_comments);
        list=new ArrayList<>();
        recyclerView=findViewById(R.id.recycler_comment_list);
        LinearLayoutManager linearLayoutManager=new LinearLayoutManager(getApplicationContext());
        recyclerView.setLayoutManager(linearLayoutManager);
        recyclerView.setHasFixedSize(true);
        getSupportActionBar().setTitle("Show Comment");
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        progressBar=findViewById(R.id.progress_comment);
        Intent i=getIntent();
        pt_id=i.getStringExtra("pt_id");
        p_id=i.getStringExtra("p_id");



    }


    @Override
    protected void onStart() {
        super.onStart();
        commentList(pt_id);

    }

    private void commentList(final String pt_id) {
        progressBar.setVisibility(View.VISIBLE);
        StringRequest request=new StringRequest(Request.Method.POST, Keys.URL.COMMENT_LIST, new Response.Listener<String>() {
            @Override
            public void onResponse(String response) {
                Loggers.i(response);
                progressBar.setVisibility(View.GONE);
                try {
                    JSONObject jsonObject=new JSONObject(response);
                    if (jsonObject.getString("success").equals("1")){
                        JSONArray jsonArray=jsonObject.getJSONArray("data");

                        for (int i=0;i<jsonArray.length();i++){
                            JSONObject jsonObject1=jsonArray.getJSONObject(i);
                            list.add(new Comment(jsonObject1.getString("ptc_id"),
                                    jsonObject1.getString("pt_id"),
                                    jsonObject1.getString("pm_id"),
                                    jsonObject1.getString("g_id"),
                                    jsonObject1.getString("s_id"),
                                    jsonObject1.getString("c_id"),
                                    jsonObject1.getString("ptc_message"),
                                    jsonObject1.getString("ptc_time"),
                                    jsonObject1.getString("pm_name"),
                                    jsonObject1.getString("g_name"),
                                    jsonObject1.getString("s_name"),
                                    jsonObject1.getString("c_name")));

                        }
                        CommentListAdapter adapter=new CommentListAdapter(getApplicationContext(),list);
                        recyclerView.setAdapter(adapter);


                    }else {
                        Toast.makeText(ShowCommentsActivity.this, "No Data Available For CommentList ", Toast.LENGTH_SHORT).show();
                    }


                } catch (JSONException e) {
                    e.printStackTrace();
                }


            }
        }, new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError error) {
                progressBar.setVisibility(View.GONE);
                error.printStackTrace();
            }
        }){
            @Override
            protected Map<String, String> getParams() throws AuthFailureError {
                Map<String,String> params=new HashMap<>();
                params.put("pt_id",pt_id);
                return params;

            }
        };

        AppController.getInstance().add(request);





    }





    @Override
    public boolean onCreateOptionsMenu(Menu menu) {

        MenuInflater inflater = getMenuInflater();
        inflater.inflate(R.menu.menu, menu);
        return true;



    }
    @Override
    public boolean onOptionsItemSelected(@NonNull MenuItem item) {
        switch (item.getItemId()){
            case android.R.id.home:
                onBackPressed();
                return true;
            case R.id.action_logout:
                if (SharedPreference.contains("h_email") && SharedPreference.contains("h_password")){
                    Intent i=new Intent(ShowCommentsActivity.this, HodLoginActivity.class);
                    i.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
                    SharedPreference.removeKey("h_email");
                    SharedPreference.removeKey("h_password");
                    startActivity(i);
                    finish();
                    break;
                }
                break;





        }

        return super.onOptionsItemSelected(item);




    }
    @Override
    public void onBackPressed() {
        Intent intent=new Intent(ShowCommentsActivity.this, HodProjectTaskActivity.class);
        intent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
        startActivity(intent);
        finish();


    }
}
