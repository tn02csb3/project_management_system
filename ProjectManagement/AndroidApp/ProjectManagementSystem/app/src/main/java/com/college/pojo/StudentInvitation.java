package com.college.pojo;

public class StudentInvitation {
    private String s_id;
    private String s_name;
    private String s_email;
    private String s_phone;
    private String s_c_id;
    private String s_department;
    private String s_year;
    private String t_id;


    public StudentInvitation(String s_id, String s_name, String s_email, String s_phone, String s_c_id, String s_department, String s_year, String t_id) {
        this.s_id = s_id;
        this.s_name = s_name;
        this.s_email = s_email;
        this.s_phone = s_phone;
        this.s_c_id = s_c_id;
        this.s_department = s_department;
        this.s_year = s_year;
        this.t_id = t_id;
    }


    public String getS_id() {
        return s_id;
    }

    public void setS_id(String s_id) {
        this.s_id = s_id;
    }

    public String getS_name() {
        return s_name;
    }

    public void setS_name(String s_name) {
        this.s_name = s_name;
    }

    public String getS_email() {
        return s_email;
    }

    public void setS_email(String s_email) {
        this.s_email = s_email;
    }

    public String getS_phone() {
        return s_phone;
    }

    public void setS_phone(String s_phone) {
        this.s_phone = s_phone;
    }

    public String getS_c_id() {
        return s_c_id;
    }

    public void setS_c_id(String s_c_id) {
        this.s_c_id = s_c_id;
    }

    public String getS_department() {
        return s_department;
    }

    public void setS_department(String s_department) {
        this.s_department = s_department;
    }

    public String getS_year() {
        return s_year;
    }

    public void setS_year(String s_year) {
        this.s_year = s_year;
    }

    public String getT_id() {
        return t_id;
    }

    public void setT_id(String t_id) {
        this.t_id = t_id;
    }
}
