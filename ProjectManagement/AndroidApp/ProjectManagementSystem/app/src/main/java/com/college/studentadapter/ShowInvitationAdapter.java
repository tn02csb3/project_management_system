package com.college.studentadapter;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.LinearLayout;
import android.widget.TextView;
import android.widget.Toast;

import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;

import com.android.volley.AuthFailureError;
import com.android.volley.Request;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.StringRequest;
import com.college.pojo.ShowInvitation;
import com.college.student.R;
import com.college.util.AppController;
import com.college.util.Keys;
import com.college.util.Loggers;
import com.college.util.SharedPreference;

import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.Map;

public class ShowInvitationAdapter extends RecyclerView.Adapter<ShowInvitationAdapter.ViewHolder> {

    private Context context;
    private ArrayList<ShowInvitation> list;

    public ShowInvitationAdapter(Context context, ArrayList<ShowInvitation> list) {
        this.context = context;
        this.list = list;
    }

    @NonNull
    @Override
    public ShowInvitationAdapter.ViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        LayoutInflater layoutInflater = LayoutInflater.from(context);
        View listItem = layoutInflater.inflate(R.layout.invitationlist, parent, false);
        listItem.setLayoutParams(new RecyclerView.LayoutParams(RecyclerView.LayoutParams.MATCH_PARENT, RecyclerView.LayoutParams.WRAP_CONTENT));

        return new ShowInvitationAdapter.ViewHolder(listItem);
    }

    @Override
    public void onBindViewHolder(@NonNull final ShowInvitationAdapter.ViewHolder holder, int position) {
        final ShowInvitation showInvitation = list.get(position);
        holder.tv_name.setText("Team Name: " + showInvitation.getT_name());
        holder.tv_in_from.setText("From: " + showInvitation.getIn_from_name());
        holder.tv_in_to.setText("To: " + showInvitation.getIn_to_name());
        holder.tv_status.setText("Status: " + showInvitation.getIn_status());


        if (showInvitation.getIn_status().equals("0")) {
            if(showInvitation.getIn_from_id().equals(SharedPreference.get("s_id"))){
                holder.tv_status.setText("Status: Pending" );
                holder.tv_status.setVisibility(View.VISIBLE);
                holder.llAction.setVisibility(View.GONE);
            }else{
                holder.tv_status.setVisibility(View.GONE);
                holder.llAction.setVisibility(View.VISIBLE);
            }
        } else {
            holder.tv_status.setVisibility(View.VISIBLE);
            holder.llAction.setVisibility(View.GONE);
            if (showInvitation.getIn_status().equals("1")) {
                holder.tv_status.setText("Status: Accepted");
            } else if (showInvitation.getIn_status().equals("2")) {
                holder.tv_status.setText("Status: Rejected");
            }
        }

        holder.btnAccept.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                updateStatus(showInvitation.getIn_id(), showInvitation.getT_id(), "1", SharedPreference.get("s_id"));
                holder.llAction.setVisibility(View.GONE);

            }
        });

        holder.btnReject.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                updateStatus(showInvitation.getIn_id(), showInvitation.getT_id(), "2", SharedPreference.get("s_id"));
                holder.llAction.setVisibility(View.GONE);
            }
        });

        /*if(showInvitation.getIn_status().equals("0")){
            holder.tv_status.setText("Status: Pending" );
        }else*/
    }

    private void updateStatus(final String in_id, final String t_id, final String s, final String s_id) {
        StringRequest request = new StringRequest(Request.Method.POST, Keys.URL.ACCEPT_INVITATION, new Response.Listener<String>() {
            @Override
            public void onResponse(String response) {
                Loggers.i(response);
                try {
                    JSONObject jsonObject=new JSONObject(response);
                    if (jsonObject.getString("success").equals("1")){

                        Toast.makeText(context, jsonObject.getString("message"), Toast.LENGTH_SHORT).show();
                        /*Intent i = new Intent(context, TeamActivity.class);
                        i.setFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
                        context.startActivity(i);*/




                    }else {
                        Toast.makeText(context, jsonObject.getString("message"), Toast.LENGTH_SHORT).show();
                    }


                } catch (JSONException e) {
                    e.printStackTrace();
                    Toast.makeText(context, "Technical problem arises", Toast.LENGTH_SHORT).show();
                }



            }
        }, new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError error) {
                error.printStackTrace();
            }
        }){
            @Override
            protected Map<String, String> getParams() throws AuthFailureError {
                Map<String, String> params = new HashMap<String, String>();
                params.put("in_id", in_id);
                params.put("t_id", t_id);
                params.put("in_status", s);
                params.put("s_id", s_id);
                return params;
            }
        };

        AppController.getInstance().add(request);
    }

    @Override
    public int getItemCount() {
        return list.size();
    }

    public class ViewHolder extends RecyclerView.ViewHolder {

        TextView tv_name, tv_in_from, tv_in_to, tv_status;
        LinearLayout llAction;
        Button btnAccept, btnReject;

        public ViewHolder(@NonNull View itemView) {
            super(itemView);
            this.tv_name = itemView.findViewById(R.id.txt_t_name);
            this.tv_in_from = itemView.findViewById(R.id.txt_t_from);
            this.tv_in_to = itemView.findViewById(R.id.txt_t_to);
            this.tv_status = itemView.findViewById(R.id.txt_t_status);
            btnAccept = itemView.findViewById(R.id.buttonAccept);
            btnReject = itemView.findViewById(R.id.buttonReject);
            llAction = itemView.findViewById(R.id.llAction);
        }
    }
}
