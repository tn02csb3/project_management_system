package com.college.pojo;

public class ShowInvitation {


    private String in_id;
    private String t_id;
    private String in_from_id;
    private String in_to_id;
    private String in_status;
    private String in_time;
    private String in_from_name;
    private String in_to_name;
    private String t_name;

    public ShowInvitation(String in_id, String t_id, String in_from_id, String in_to_id, String in_status, String in_time, String in_from_name, String in_to_name, String t_name) {
        this.in_id = in_id;
        this.t_id = t_id;
        this.in_from_id = in_from_id;
        this.in_to_id = in_to_id;
        this.in_status = in_status;
        this.in_time = in_time;
        this.in_from_name = in_from_name;
        this.in_to_name = in_to_name;
        this.t_name = t_name;
    }

    public String getIn_id() {
        return in_id;
    }

    public void setIn_id(String in_id) {
        this.in_id = in_id;
    }

    public String getT_id() {
        return t_id;
    }

    public void setT_id(String t_id) {
        this.t_id = t_id;
    }

    public String getIn_from_id() {
        return in_from_id;
    }

    public void setIn_from_id(String in_from_id) {
        this.in_from_id = in_from_id;
    }

    public String getIn_to_id() {
        return in_to_id;
    }

    public void setIn_to_id(String in_to_id) {
        this.in_to_id = in_to_id;
    }

    public String getIn_status() {
        return in_status;
    }

    public void setIn_status(String in_status) {
        this.in_status = in_status;
    }

    public String getIn_time() {
        return in_time;
    }

    public void setIn_time(String in_time) {
        this.in_time = in_time;
    }

    public String getIn_from_name() {
        return in_from_name;
    }

    public void setIn_from_name(String in_from_name) {
        this.in_from_name = in_from_name;
    }

    public String getIn_to_name() {
        return in_to_name;
    }

    public void setIn_to_name(String in_to_name) {
        this.in_to_name = in_to_name;
    }

    public String getT_name() {
        return t_name;
    }

    public void setT_name(String t_name) {
        this.t_name = t_name;
    }
}
