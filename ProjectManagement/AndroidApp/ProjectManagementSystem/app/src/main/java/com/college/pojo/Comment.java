package com.college.pojo;

public class Comment {

    private String ptc_id;
    private String pt_id;
    private String pm_id;
    private String g_id;
    private String s_id;
    private String c_id;
    private String ptc_message;
    private String ptc_time;
    private String pm_name;
    private String g_name;
    private String s_name;
    private String c_name;


    public Comment(String ptc_id, String pt_id, String pm_id, String g_id, String s_id, String c_id, String ptc_message, String ptc_time, String pm_name, String g_name, String s_name, String c_name) {
        this.ptc_id = ptc_id;
        this.pt_id = pt_id;
        this.pm_id = pm_id;
        this.g_id = g_id;
        this.s_id = s_id;
        this.c_id = c_id;
        this.ptc_message = ptc_message;
        this.ptc_time = ptc_time;
        this.pm_name = pm_name;
        this.g_name = g_name;
        this.s_name = s_name;
        this.c_name = c_name;
    }


    public String getPtc_id() {
        return ptc_id;
    }

    public void setPtc_id(String ptc_id) {
        this.ptc_id = ptc_id;
    }

    public String getPt_id() {
        return pt_id;
    }

    public void setPt_id(String pt_id) {
        this.pt_id = pt_id;
    }

    public String getPm_id() {
        return pm_id;
    }

    public void setPm_id(String pm_id) {
        this.pm_id = pm_id;
    }

    public String getG_id() {
        return g_id;
    }

    public void setG_id(String g_id) {
        this.g_id = g_id;
    }

    public String getS_id() {
        return s_id;
    }

    public void setS_id(String s_id) {
        this.s_id = s_id;
    }

    public String getC_id() {
        return c_id;
    }

    public void setC_id(String c_id) {
        this.c_id = c_id;
    }

    public String getPtc_message() {
        return ptc_message;
    }

    public void setPtc_message(String ptc_message) {
        this.ptc_message = ptc_message;
    }

    public String getPtc_time() {
        return ptc_time;
    }

    public void setPtc_time(String ptc_time) {
        this.ptc_time = ptc_time;
    }

    public String getPm_name() {
        return pm_name;
    }

    public void setPm_name(String pm_name) {
        this.pm_name = pm_name;
    }

    public String getG_name() {
        return g_name;
    }

    public void setG_name(String g_name) {
        this.g_name = g_name;
    }

    public String getS_name() {
        return s_name;
    }

    public void setS_name(String s_name) {
        this.s_name = s_name;
    }

    public String getC_name() {
        return c_name;
    }

    public void setC_name(String c_name) {
        this.c_name = c_name;
    }
}
