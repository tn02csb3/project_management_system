package com.college.student;

import androidx.appcompat.app.AppCompatActivity;

import android.content.Intent;
import android.os.Bundle;
import android.util.Patterns;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ProgressBar;
import android.widget.Toast;

import com.android.volley.AuthFailureError;
import com.android.volley.Request;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.StringRequest;
import com.college.util.AppController;
import com.college.util.Keys;
import com.college.util.SharedPreference;

import org.json.JSONException;
import org.json.JSONObject;

import java.util.HashMap;
import java.util.Map;

public class LoginActivity extends AppCompatActivity {

    Button button_login,button_register;
    EditText editText_email,editText_pass;
    ProgressBar progressBar;



    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_login);

        editText_email=findViewById(R.id.edt_Email);
        editText_pass=findViewById(R.id.edt_Pass);
        button_login=findViewById(R.id.btn_login);
        progressBar=findViewById(R.id.progressBar_u_login);
        button_register=findViewById(R.id.btn_register);
        getSupportActionBar().setTitle("Login");
        button_login.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                String s_email=editText_email.getText().toString().trim();
                String s_password=editText_pass.getText().toString().trim();
                if (s_email.equals("") || s_password.equals("")){
                    Toast.makeText(LoginActivity.this, "please fill all the details", Toast.LENGTH_SHORT).show();
                }else if(!Patterns.EMAIL_ADDRESS.matcher(s_email).matches()){
                    editText_email.setError("Invalid Email ID");
                }else {
                    login(s_email,s_password);
                }
            }
        });

        button_register.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent=new Intent(LoginActivity.this,RegisterActivity.class);
                startActivity(intent);
            }
        });

    }

    private void login(final String s_email, final String s_password) {
        progressBar.setVisibility(View.VISIBLE);
        StringRequest request=new StringRequest(Request.Method.POST, Keys.URL.STUDENT_LOGIN, new Response.Listener<String>() {
            @Override
            public void onResponse(String response) {
                progressBar.setVisibility(View.GONE);
                try {
                    JSONObject jsonObject=new JSONObject(response);
                    if (jsonObject.getString("success").equals("1")){
                        JSONObject jsonObject1=jsonObject.getJSONObject("data");

                        String s_id=jsonObject1.getString("s_id");
                        Toast.makeText(LoginActivity.this, jsonObject.getString("message"), Toast.LENGTH_SHORT).show();
                        Intent intent=new Intent(LoginActivity.this, TeamActivity.class);
                        intent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
                        SharedPreference.save("s_email",s_email);
                        SharedPreference.save("s_password",s_password);
                        SharedPreference.save("s_id",s_id);
                        startActivity(intent);
                        clear();
                        finish();

                    }else {
                        Toast.makeText(LoginActivity.this, jsonObject.getString("message"), Toast.LENGTH_SHORT).show();
                    }


                } catch (JSONException e) {
                    e.printStackTrace();
                    Toast.makeText(LoginActivity.this, "Technical problem arises", Toast.LENGTH_SHORT).show();
                }

            }
        }, new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError error) {
                progressBar.setVisibility(View.GONE);
                error.printStackTrace();
            }
        }){
            @Override
            protected Map<String, String> getParams() throws AuthFailureError {
                Map<String,String> params=new HashMap<>();
                params.put("s_email",s_email);
                params.put("s_password",s_password);
                return params;
            }
        };
        AppController.getInstance().add(request);



    }

    private void clear() {

        editText_email.setText("");
        editText_pass.setText("");
    }

    @Override
    public void onBackPressed() {
        Intent intent=new Intent(LoginActivity.this,MenuActivity.class);
        intent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
        startActivity(intent);
        finish();

    }
}
