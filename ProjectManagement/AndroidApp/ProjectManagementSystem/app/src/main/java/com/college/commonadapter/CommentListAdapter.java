package com.college.commonadapter;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.cardview.widget.CardView;
import androidx.recyclerview.widget.RecyclerView;

import com.college.pojo.Comment;
import com.college.student.R;

import java.util.ArrayList;

public class CommentListAdapter extends RecyclerView.Adapter<CommentListAdapter.ViewHolder> {

    private Context context;
    private ArrayList<Comment> list;

    public CommentListAdapter(Context context, ArrayList<Comment> list) {
        this.context = context;
        this.list = list;
    }

    @NonNull
    @Override
    public ViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        LayoutInflater layoutInflater = LayoutInflater.from(context);
        View listItem = layoutInflater.inflate(R.layout.comment, parent, false);
        listItem.setLayoutParams(new RecyclerView.LayoutParams(RecyclerView.LayoutParams.MATCH_PARENT, RecyclerView.LayoutParams.WRAP_CONTENT));
        return new CommentListAdapter.ViewHolder(listItem);


    }

    @Override
    public void onBindViewHolder(@NonNull ViewHolder holder, int position) {
        final Comment comment=list.get(position);
        holder.tv_title.setText("Title: "+comment.getPtc_message());
        holder.tv_status.setText(comment.getPtc_time());




        if (comment.getPm_name().equals("0")){

        }else if(comment.getPm_id().equals("1")){
            holder.tv_comment.setText("Comment By: "+comment.getPm_name());
        }

        if(comment.getG_id().equals("0")){

        }else if(comment.getG_id().equals(comment.getG_id())){
            holder.tv_comment.setText("Comment By: "+comment.getG_name());
        }

        if(comment.getS_id().equals("0")){

        }else if(comment.getS_id().equals(comment.getS_id())){
            holder.tv_comment.setText("Comment By: "+comment.getS_name());
        }

        if(comment.getC_id().equals("0")){

        }else if(comment.getC_id().equals(comment.getC_id())){
            holder.tv_comment.setText("Comment By: "+comment.getC_name());
        }



    }

    @Override
    public int getItemCount() {

        return list.size();
    }

    public class ViewHolder extends RecyclerView.ViewHolder {

        TextView tv_title,tv_status,tv_comment;
        CardView cd;
        ImageView imageView;


        public ViewHolder(@NonNull View itemView) {
            super(itemView);
            this.tv_title=itemView.findViewById(R.id.txt_c_title);
            this.tv_status=itemView.findViewById(R.id.txt_c_time);
            this.cd=itemView.findViewById(R.id.project_card);
            this.tv_comment=itemView.findViewById(R.id.txt_comment);



        }
    }
}
