package com.college.student;
import androidx.appcompat.app.AppCompatActivity;
import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;


import com.college.admin.AdminLoginActivity;
import com.college.admin.AdminMenuActivity;
import com.college.faculty.FacultyLoginActivity;
import com.college.guide.GuideLoginActivity;
import com.college.guide.GuideProjectListActivity;
import com.college.hod.HODProjectActivity;
import com.college.hod.HodLoginActivity;
import com.college.mentor.MentorLoginActivity;
import com.college.faculty.ProjectActivity;
import com.college.mentor.MentorProjectListActivity;
import com.college.projectcreator.CreatorProjectListActivity;
import com.college.projectcreator.ProjectCreatorLoginActivity;
import com.college.util.SharedPreference;

public class MenuActivity extends AppCompatActivity {

    Button button_student,button_admin,button_guide,button_mentor,button_faculty,button_project,button_hod;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_menu);
        button_admin=findViewById(R.id.btn_admin);
        button_guide=findViewById(R.id.btn_guide);
        button_mentor=findViewById(R.id.btn_mentor);
        button_faculty=findViewById(R.id.btn_faculty);
        button_project=findViewById(R.id.btn_project);
        button_hod=findViewById(R.id.btn_hod);
        button_student=findViewById(R.id.btn_student);
        getSupportActionBar().setTitle("Menu");
        button_guide.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (SharedPreference.contains("g_email") && SharedPreference.contains("g_password") && SharedPreference.contains("g_id")){
                    Intent intent=new Intent(MenuActivity.this, GuideProjectListActivity.class);
                    intent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
                    startActivity(intent);
                    finish();


                }else {
                    Intent intent = new Intent(MenuActivity.this, GuideLoginActivity.class);
                    startActivity(intent);
                }
            }
        });

        button_mentor.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (SharedPreference.contains("pm_email") && SharedPreference.contains("pm_password") && SharedPreference.contains("pm_id")){
                    Intent intent=new Intent(MenuActivity.this, MentorProjectListActivity.class);
                    intent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
                    startActivity(intent);
                    finish();
                }else {
                    Intent intent = new Intent(MenuActivity.this, MentorLoginActivity.class);
                    startActivity(intent);
                }
            }
        });

        button_faculty.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (SharedPreference.contains("c_email") && SharedPreference.contains("c_password") && SharedPreference.contains("c_id") ){
                    Intent intent=new Intent(MenuActivity.this, ProjectActivity.class);
                    intent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
                    startActivity(intent);
                    finish();
                }else {
                    Intent intent=new Intent(MenuActivity.this, FacultyLoginActivity.class);
                    startActivity(intent);
                }


            }
        });
        button_hod.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (SharedPreference.contains("h_email") && SharedPreference.contains("h_password")){
                    Intent intent=new Intent(MenuActivity.this, HODProjectActivity.class);
                    intent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
                    startActivity(intent);
                    finish();
                }else {
                    Intent intent = new Intent(MenuActivity.this, HodLoginActivity.class);
                    startActivity(intent);
                }
            }
        });
        button_project.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (SharedPreference.contains("pc_email") && SharedPreference.contains("pc_password") && SharedPreference.contains("pc_id")){
                    Intent intent=new Intent(MenuActivity.this, CreatorProjectListActivity.class);
                    intent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
                    startActivity(intent);
                    finish();
                }else {
                    Intent intent=new Intent(MenuActivity.this, ProjectCreatorLoginActivity.class);
                    startActivity(intent);
                }
            }
        });


        button_admin.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (SharedPreference.contains("a_email") && SharedPreference.contains("a_password")){
                    Intent intent=new Intent(MenuActivity.this, AdminMenuActivity.class);
                    intent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
                    startActivity(intent);
                    finish();
                }else {
                    Intent intent=new Intent(MenuActivity.this, AdminLoginActivity.class);
                    startActivity(intent);
                }


            }
        });

        button_student.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (SharedPreference.contains("s_email") && SharedPreference.contains("s_password") && SharedPreference.contains("s_id")){
                    Intent intent=new Intent(MenuActivity.this, TeamActivity.class);
                    intent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
                    startActivity(intent);
                    finish();
                }else {
                    Intent intent=new Intent(MenuActivity.this,LoginActivity.class);
                    startActivity(intent);
                }

            }
        });

    }

    @Override
    public void onBackPressed() {

        super.onBackPressed();
    }


}
