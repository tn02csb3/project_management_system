package com.college.student;

import androidx.appcompat.app.AppCompatActivity;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;
import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import android.widget.ProgressBar;
import android.widget.TextView;
import android.widget.Toast;

import com.android.volley.AuthFailureError;
import com.android.volley.Request;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.StringRequest;
import com.college.adminadapter.UserAdapter;
import com.college.pojo.RecyclerItemClickListener;
import com.college.pojo.Student;
import com.college.pojo.StudentInvitation;
import com.college.studentadapter.StudentInvitationAdapter;
import com.college.util.AppController;
import com.college.util.Keys;
import com.college.util.Loggers;
import com.college.util.SharedPreference;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.Map;

public class InvitationActivity extends AppCompatActivity {

    TextView tv_name;
    RecyclerView recyclerView;
    String t_id,s_id,t_name;
    ArrayList<StudentInvitation> list;
    ProgressBar progressBar;



    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_invitation);
        tv_name=findViewById(R.id.txt_t_name);
        recyclerView=findViewById(R.id.recycler_user_list);
        getSupportActionBar().setTitle("Student List");
        progressBar=findViewById(R.id.progressbar_list);
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        LinearLayoutManager layoutManager=new LinearLayoutManager(getApplicationContext());
        recyclerView.setLayoutManager(layoutManager);
        recyclerView.setHasFixedSize(true);
        list=new ArrayList<StudentInvitation>();
        Intent i=getIntent();
        t_id=i.getStringExtra("t_id");
        Loggers.i(String.valueOf(t_id));
        s_id= SharedPreference.get("s_id");
        t_name=i.getStringExtra("t_name");
        tv_name.setText("Team Name: " +t_name);

        recyclerView.addOnItemTouchListener(new RecyclerItemClickListener(getApplicationContext(), recyclerView, new RecyclerItemClickListener.OnItemClickListener() {
            @Override
            public void onItemClick(View view, int position) {
                String in_to_id = list.get(position).getS_id();
                sendInvitation(t_id, s_id, in_to_id);
            }

            @Override
            public void onLongItemClick(View view, int position) {

            }
        }));


    }

    private void sendInvitation(final String t_id, final String s_id, final String in_to_id) {
        progressBar.setVisibility(View.VISIBLE);
        StringRequest request = new StringRequest(Request.Method.POST, Keys.URL.SEND_INVITAION, new Response.Listener<String>() {
            @Override
            public void onResponse(String response) {
               progressBar.setVisibility(View.GONE);
                try {
                    JSONObject jsonObject=new JSONObject(response);
                    if (jsonObject.getString("success").equals("1")){
                        Toast.makeText(InvitationActivity.this, jsonObject.getString("message"), Toast.LENGTH_SHORT).show();

                    }else {
                        Toast.makeText(InvitationActivity.this, jsonObject.getString("message"), Toast.LENGTH_SHORT).show();
                    }


                } catch (JSONException e) {
                    e.printStackTrace();
                    Toast.makeText(InvitationActivity.this, "Technical problem arises", Toast.LENGTH_SHORT).show();
                }

            }
        }, new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError error) {
                progressBar.setVisibility(View.GONE);
                error.printStackTrace();
            }
        }){
            @Override
            protected Map<String, String> getParams() throws AuthFailureError {
                Map<String, String> params = new HashMap<String, String>();
                params.put("t_id", t_id);
                params.put("in_from_id", s_id);
                params.put("in_to_id", in_to_id);
                return params;
            }
        };

        AppController.getInstance().add(request);
    }

    @Override
    protected void onStart() {
        super.onStart();
        showList(SharedPreference.get("s_id"));
    }

    private void showList(final String s_id) {
        progressBar.setVisibility(View.VISIBLE);
        StringRequest request=new StringRequest(Request.Method.POST, Keys.URL.GET_STUDENT_LIST, new Response.Listener<String>() {
            @Override
            public void onResponse(String response) {
                Loggers.i(response);
                progressBar.setVisibility(View.GONE);
                try {
                    JSONObject jsonObject=new JSONObject(response);
                    if (jsonObject.getString("success").equals("1")){
                        JSONArray jsonArray=jsonObject.getJSONArray("data");

                        for (int i=0;i<jsonArray.length();i++){
                            JSONObject jsonObject1=jsonArray.getJSONObject(i);
                            list.add(new StudentInvitation(jsonObject1.getString("s_id"),
                                    jsonObject1.getString("s_name"),
                                    jsonObject1.getString("s_email"),
                                    jsonObject1.getString("s_phone"),
                                    jsonObject1.getString("s_c_id"),
                                    jsonObject1.getString("s_department"),
                                    jsonObject1.getString("s_year"),
                                    jsonObject1.getString("t_id")));


                        }
                        StudentInvitationAdapter adapter=new StudentInvitationAdapter(getApplicationContext(),list);
                        recyclerView.setAdapter(adapter);

                    }else {

                        Toast.makeText(InvitationActivity.this, "No Data Available for student", Toast.LENGTH_SHORT).show();
                    }


                } catch (JSONException e) {
                    e.printStackTrace();
                }


            }
        }, new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError error) {
                progressBar.setVisibility(View.GONE);
                error.printStackTrace();
            }
        }){
            @Override
            protected Map<String, String> getParams() throws AuthFailureError {
                Map<String,String> params=new HashMap<>();
                params.put("s_id",s_id);
                return params;
            }
        };

        AppController.getInstance().add(request);




    }
}
