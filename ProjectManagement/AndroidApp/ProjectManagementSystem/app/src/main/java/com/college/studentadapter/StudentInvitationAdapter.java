package com.college.studentadapter;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.cardview.widget.CardView;
import androidx.recyclerview.widget.RecyclerView;

import com.college.pojo.Student;
import com.college.pojo.StudentInvitation;
import com.college.student.R;

import java.util.ArrayList;

public class StudentInvitationAdapter extends RecyclerView.Adapter<StudentInvitationAdapter.ViewHolder> {

    private Context context;
    private ArrayList<StudentInvitation> list;

    public StudentInvitationAdapter(Context context, ArrayList<StudentInvitation> list) {
        this.context = context;
        this.list = list;
    }

    @NonNull
    @Override
    public StudentInvitationAdapter.ViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        LayoutInflater layoutInflater = LayoutInflater.from(context);
        View listItem = layoutInflater.inflate(R.layout.userlist, parent, false);
        listItem.setLayoutParams(new RecyclerView.LayoutParams(RecyclerView.LayoutParams.MATCH_PARENT, RecyclerView.LayoutParams.WRAP_CONTENT));

        return new StudentInvitationAdapter.ViewHolder(listItem);
    }

    @Override
    public void onBindViewHolder(@NonNull StudentInvitationAdapter.ViewHolder holder, int position) {
        StudentInvitation studentInvitation = list.get(position);
        holder.tv_name.setText("Name: " + studentInvitation.getS_name());
        holder.tv_email.setText("Email: " +studentInvitation.getS_email());
        holder.tv_phone.setText("Phone: " + studentInvitation.getS_phone());
        holder.tv_id.setText("College ID: " + studentInvitation.getS_c_id());
        holder.tv_dept.setText("Department: " + studentInvitation.getS_department());
        holder.tv_year.setText("Year: " + studentInvitation.getS_year());







    }

    @Override
    public int getItemCount() {
        return list.size();
    }

    public class ViewHolder extends RecyclerView.ViewHolder {

        TextView tv_name, tv_email, tv_phone, tv_id, tv_dept, tv_year;
        Button button_send_invitation;
        CardView cd;


        public ViewHolder(@NonNull View itemView) {
            super(itemView);
            this.tv_name = itemView.findViewById(R.id.txt_name);
            this.tv_email = itemView.findViewById(R.id.txt_email);
            this.tv_phone = itemView.findViewById(R.id.txt_phone);
            this.tv_id = itemView.findViewById(R.id.txt_college_id);
            this.tv_dept = itemView.findViewById(R.id.txt_department);
            this.tv_year = itemView.findViewById(R.id.txt_year);
            cd=itemView.findViewById(R.id.card_view);


        }
    }
}
