package com.college.admin;

import androidx.annotation.NonNull;
import androidx.appcompat.app.AppCompatActivity;

import android.content.Intent;
import android.os.Bundle;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.view.View;
import android.widget.Button;

import com.college.student.MenuActivity;
import com.college.student.R;
import com.college.util.SharedPreference;

public class AdminMenuActivity extends AppCompatActivity {

    Button button_student_list,button_create_batch,button_project_list;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_admin_menu);
        getSupportActionBar().setTitle("Admin Menu");
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        button_student_list=findViewById(R.id.btn_student_list);
        //button_create_batch=findViewById(R.id.btn_create_batch);
        button_project_list=findViewById(R.id.btn_project_list);

        button_student_list.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent=new Intent(AdminMenuActivity.this,UserListActivity.class);
                startActivity(intent);

            }
        });

        button_project_list.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent=new Intent(AdminMenuActivity.this,ProjectListActivity.class);
                startActivity(intent);

            }
        });
    }


    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        MenuInflater inflater=getMenuInflater();
        inflater.inflate(R.menu.menu,menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(@NonNull MenuItem item) {
        switch (item.getItemId()){
            case android.R.id.home:

                onBackPressed();
                break;
            case R.id.action_logout:
                if (SharedPreference.contains("a_email") && SharedPreference.contains("a_password")){
                    Intent intent=new Intent(AdminMenuActivity.this,AdminLoginActivity.class);
                    intent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
                    SharedPreference.removeKey("a_email");
                    SharedPreference.removeKey("a_password");
                    startActivity(intent);
                    finish();
                }
        }

        return super.onOptionsItemSelected(item);

    }

    @Override
    public void onBackPressed() {
        Intent intent=new Intent(AdminMenuActivity.this, MenuActivity.class);
        intent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
        startActivity(intent);
        finish();

    }

}
