package com.college.studentadapter;

import android.content.Context;
import android.content.Intent;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toast;

import androidx.annotation.NonNull;
import androidx.cardview.widget.CardView;
import androidx.recyclerview.widget.RecyclerView;

import com.college.pojo.Task;
import com.college.student.AddCommentActivity;
import com.college.student.R;

import java.util.ArrayList;

public class RunningProjectTaskAdapter extends RecyclerView.Adapter<RunningProjectTaskAdapter.ViewHolder> {

    private Context context;
    private ArrayList<Task> list;

    public RunningProjectTaskAdapter(Context context, ArrayList<Task> list) {
        this.context = context;
        this.list = list;
    }

    @NonNull
    @Override
    public ViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        LayoutInflater layoutInflater = LayoutInflater.from(context);
        View listItem = layoutInflater.inflate(R.layout.tasklist, parent, false);
        listItem.setLayoutParams(new RecyclerView.LayoutParams(RecyclerView.LayoutParams.MATCH_PARENT, RecyclerView.LayoutParams.WRAP_CONTENT));
        return new RunningProjectTaskAdapter.ViewHolder(listItem);


    }

    @Override
    public void onBindViewHolder(@NonNull ViewHolder holder, int position) {
        final Task task=list.get(position);
        holder.tv_title.setText("Title: "+task.getPt_title());
        holder.tv_status.setText("Status: "+task.getPt_status());

        if (task.getPt_status().equals("1")){
            holder.tv_status.setText("Status: " + "Running");
            holder.cd.setVisibility(View.VISIBLE);
        }else{
           // Toast.makeText(context, "Task is not started", Toast.LENGTH_LONG).show();
            holder.cd.setVisibility(View.GONE);
        }








    }

    @Override
    public int getItemCount() {

        return list.size();
    }

    public class ViewHolder extends RecyclerView.ViewHolder {

        TextView tv_title,tv_status;
        CardView cd;
        ImageView imageView;


        public ViewHolder(@NonNull View itemView) {
            super(itemView);
            this.tv_title=itemView.findViewById(R.id.txt_title);
            this.tv_status=itemView.findViewById(R.id.txt_status);
            this.cd=itemView.findViewById(R.id.card_view);





        }
    }
}
