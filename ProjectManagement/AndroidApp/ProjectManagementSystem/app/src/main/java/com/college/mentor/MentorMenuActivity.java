package com.college.mentor;

import androidx.annotation.NonNull;
import androidx.appcompat.app.AppCompatActivity;

import android.content.Intent;
import android.os.Bundle;
import android.view.MenuItem;
import android.view.View;
import android.widget.Button;

import com.college.faculty.SeeProjectTaskActivity;
import com.college.student.R;
import com.college.util.SharedPreference;

public class MentorMenuActivity extends AppCompatActivity {

    Button button_see_project,button_project_task,button_logout;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_mentor_menu);
        getSupportActionBar().setTitle("Menu");
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        button_see_project=findViewById(R.id.btn_see_project);
        button_project_task=findViewById(R.id.btn_add_project_task);
        button_logout=findViewById(R.id.btn_mentor_logout);
        button_see_project.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent=new Intent(MentorMenuActivity.this, MentorProjectListActivity.class);
                startActivity(intent);
            }
        });
        button_project_task.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent=new Intent(MentorMenuActivity.this, SeeProjectTaskActivity.class);
                startActivity(intent);
            }
        });
        button_logout.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (SharedPreference.contains("pm_email") && SharedPreference.contains("pm_password")){
                    SharedPreference.removeKey("pm_email");
                    SharedPreference.removeKey("pm_password");
                    Intent intent=new Intent(MentorMenuActivity.this,MentorLoginActivity.class);
                    startActivity(intent);
                    finish();


                }
            }
        });


    }

    @Override
    public boolean onOptionsItemSelected(@NonNull MenuItem item) {
        switch (item.getItemId()){
            case android.R.id.home:

                onBackPressed();
                break;
        }

        return super.onOptionsItemSelected(item);
    }
}
