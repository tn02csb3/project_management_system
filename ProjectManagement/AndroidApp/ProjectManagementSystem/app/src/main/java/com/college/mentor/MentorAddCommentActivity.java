package com.college.mentor;

import androidx.annotation.NonNull;
import androidx.appcompat.app.AppCompatActivity;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import android.content.Intent;
import android.os.Bundle;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ProgressBar;
import android.widget.Toast;

import com.android.volley.AuthFailureError;
import com.android.volley.Request;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.StringRequest;
import com.college.commonadapter.CommentListAdapter;
import com.college.guide.ChangeStatusActivity;
import com.college.pojo.Comment;
import com.college.student.MenuActivity;
import com.college.student.R;
import com.college.util.AppController;
import com.college.util.Keys;
import com.college.util.Loggers;
import com.college.util.SharedPreference;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.Map;

public class MentorAddCommentActivity extends AppCompatActivity {

    EditText editText_comment;
    Button button_add;
    ProgressBar progressBar;
    RecyclerView recyclerView;
    ArrayList<Comment> list;
    String pt_id,p_id;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_add_comments);
        list=new ArrayList<>();
        recyclerView=findViewById(R.id.recycler_comment_list);
        LinearLayoutManager linearLayoutManager=new LinearLayoutManager(getApplicationContext());
        recyclerView.setLayoutManager(linearLayoutManager);
        recyclerView.setHasFixedSize(true);
        getSupportActionBar().setTitle("Add Comment");
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        editText_comment=findViewById(R.id.edt_comment);
        button_add=findViewById(R.id.btn_add_comment);
        progressBar=findViewById(R.id.progress_add_comment);
        Intent i=getIntent();
        pt_id=i.getStringExtra("pt_id");
        final String pm_id= SharedPreference.get("pm_id");
        Intent i1=getIntent();
        p_id=i1.getStringExtra("p_id");


        button_add.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                String ptc_message=editText_comment.getText().toString().trim();
                if (ptc_message.equals("")){
                    Toast.makeText(MentorAddCommentActivity.this, "Please fill all the details", Toast.LENGTH_SHORT).show();
                }else {
                    addComment(pt_id,ptc_message,pm_id);
                }
            }
        });



    }


    @Override
    protected void onStart() {
        super.onStart();
        commentList(pt_id);

    }

    private void commentList(final String pt_id) {
        progressBar.setVisibility(View.VISIBLE);
        StringRequest request=new StringRequest(Request.Method.POST, Keys.URL.COMMENT_LIST, new Response.Listener<String>() {
            @Override
            public void onResponse(String response) {
                Loggers.i(response);
                progressBar.setVisibility(View.GONE);
                try {
                    JSONObject jsonObject=new JSONObject(response);
                    if (jsonObject.getString("success").equals("1")){
                        JSONArray jsonArray=jsonObject.getJSONArray("data");

                        for (int i=0;i<jsonArray.length();i++){
                            JSONObject jsonObject1=jsonArray.getJSONObject(i);
                            list.add(new Comment(jsonObject1.getString("ptc_id"),
                                    jsonObject1.getString("pt_id"),
                                    jsonObject1.getString("pm_id"),
                                    jsonObject1.getString("g_id"),
                                    jsonObject1.getString("s_id"),
                                    jsonObject1.getString("c_id"),
                                    jsonObject1.getString("ptc_message"),
                                    jsonObject1.getString("ptc_time"),
                                    jsonObject1.getString("pm_name"),
                                    jsonObject1.getString("g_name"),
                                    jsonObject1.getString("s_name"),
                                    jsonObject1.getString("c_name")));

                        }
                        CommentListAdapter adapter=new CommentListAdapter(getApplicationContext(),list);
                        recyclerView.setAdapter(adapter);


                    }else {
                        Toast.makeText(MentorAddCommentActivity.this, "No Data Available For Comment List", Toast.LENGTH_SHORT).show();
                    }


                } catch (JSONException e) {
                    e.printStackTrace();
                }


            }
        }, new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError error) {
                progressBar.setVisibility(View.GONE);
                error.printStackTrace();
            }
        }){
            @Override
            protected Map<String, String> getParams() throws AuthFailureError {
                Map<String,String> params=new HashMap<>();
                params.put("pt_id",pt_id);
                return params;

            }
        };

        AppController.getInstance().add(request);





    }


    private void addComment(final String pt_id, final String ptc_message, final String pm_id){
        progressBar.setVisibility(View.VISIBLE);
        StringRequest request=new StringRequest(Request.Method.POST, Keys.URL.ADD_COMMENT, new Response.Listener<String>() {
            @Override
            public void onResponse(String response) {
                progressBar.setVisibility(View.GONE);
                try {
                    JSONObject jsonObject=new JSONObject(response);
                    if (jsonObject.getString("success").equals("1")){

                        Toast.makeText(MentorAddCommentActivity.this, jsonObject.getString("message"), Toast.LENGTH_SHORT).show();
                        clear();


                    }else {
                        Toast.makeText(MentorAddCommentActivity.this, jsonObject.getString("message"), Toast.LENGTH_SHORT).show();
                    }


                } catch (JSONException e) {
                    e.printStackTrace();
                    Toast.makeText(MentorAddCommentActivity.this, "Technical problem arises", Toast.LENGTH_SHORT).show();
                }


            }
        }, new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError error) {
                progressBar.setVisibility(View.GONE);
                error.printStackTrace();
            }
        }){

            @Override
            protected Map<String, String> getParams() throws AuthFailureError {
                Map<String,String> params=new HashMap<>();
                params.put("pt_id",pt_id);
                params.put("g_id","0");
                params.put("ptc_message",ptc_message);
                params.put("pm_id",pm_id);
                params.put("s_id","0");
                params.put("c_id","0");
                return params;
            }
        };
        AppController.getInstance().add(request);

    }

    private void clear() {
        editText_comment.setText("");
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        MenuInflater inflater=getMenuInflater();
        inflater.inflate(R.menu.guidemenu,menu);
        return true;
    }




    @Override
    public boolean onOptionsItemSelected(@NonNull MenuItem item) {
        switch (item.getItemId()){
            case android.R.id.home:
                onBackPressed();
                return true;
            case R.id.logout:
                if (SharedPreference.contains("pm_email") && SharedPreference.contains("pm_password") && SharedPreference.contains("pm_id")){
                    Intent i=new Intent(MentorAddCommentActivity.this, MentorLoginActivity.class);
                    i.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
                    SharedPreference.removeKey("pm_email");
                    SharedPreference.removeKey("pm_password");
                    SharedPreference.removeKey("pm_id");
                    startActivity(i);
                    finish();
                    break;
                }
                break;


            case R.id.change_status:
                Intent intent=new Intent(MentorAddCommentActivity.this, ChangeStatusActivity.class);
                intent.putExtra("p_id",p_id);
                startActivity(intent);


        }

        return super.onOptionsItemSelected(item);


    }
    @Override
    public void onBackPressed() {
        Intent intent=new Intent(MentorAddCommentActivity.this, MentorProjectTaskListActivity.class);
        intent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
        startActivity(intent);
        finish();
    }
}
