package com.college.fragment;


import androidx.annotation.NonNull;
import androidx.fragment.app.Fragment;
import androidx.fragment.app.FragmentManager;
import androidx.fragment.app.FragmentStatePagerAdapter;


public class PagerAdapter extends FragmentStatePagerAdapter {

    int position;

    public PagerAdapter(@NonNull FragmentManager fm, int countTab) {
        super(fm,countTab);

    }

    @NonNull
    @Override
    public Fragment getItem(int i) {
        switch (i){
            case 0:
                ToDoListFragment taskListFragment=new ToDoListFragment();
                return taskListFragment;
            case 1:
                RunningFragment secondFragment=new RunningFragment();
                return secondFragment;
            case 2:
                CompleteFragment thirdFragment=new CompleteFragment();
                return thirdFragment;
            default:
                return null;
        }

    }

    @Override
    public int getCount() {

        return position;
    }

//    @StringRes
//    private static final int[] TAB_TITLES = new int[]{R.string.tab_text_1, R.string.tab_text_2};
//    private finol Context mContext;
//
//    public PagerAdapter(Context context, FragmentManager fm) {
//        super(fm);
//        mContext = context;
//    }
//
//    @Override
//    public Fragment getItem(int position) {

////        return PlaceholderFragment.newInstance(position + 1);
//    }
//
//    @Nullable
//    @Override
//    public CharSequence getPageTitle(int position) {
//        return mContext.getResources().getString(TAB_TITLES[position]);
//    }
//
//    @Override
//    public int getCount() {
//        // Show 2 total pages.
//        return 3 ;
//    }
}