package com.college.pojo;

public class Project {
    private String p_id;
    private String p_name;
    private String p_description;
    private String p_domain;
    private String p_start;
    private String p_end;
    private String g_id;
    private String t_id;

    public Project(String p_id, String p_name, String p_description, String p_domain, String p_start, String p_end, String g_id, String t_id) {
        this.p_id = p_id;
        this.p_name = p_name;
        this.p_description = p_description;
        this.p_domain = p_domain;
        this.p_start = p_start;
        this.p_end = p_end;
        this.g_id = g_id;
        this.t_id = t_id;
    }


    public String getP_id() {
        return p_id;
    }

    public void setP_id(String p_id) {
        this.p_id = p_id;
    }

    public String getP_name() {
        return p_name;
    }

    public void setP_name(String p_name) {
        this.p_name = p_name;
    }

    public String getP_description() {
        return p_description;
    }

    public void setP_description(String p_description) {
        this.p_description = p_description;
    }

    public String getP_domain() {
        return p_domain;
    }

    public void setP_domain(String p_domain) {
        this.p_domain = p_domain;
    }

    public String getP_start() {
        return p_start;
    }

    public void setP_start(String p_start) {
        this.p_start = p_start;
    }

    public String getP_end() {
        return p_end;
    }

    public void setP_end(String p_end) {
        this.p_end = p_end;
    }

    public String getG_id() {
        return g_id;
    }

    public void setG_id(String g_id) {
        this.g_id = g_id;
    }

    public String getT_id() {
        return t_id;
    }

    public void setT_id(String t_id) {
        this.t_id = t_id;
    }
}
