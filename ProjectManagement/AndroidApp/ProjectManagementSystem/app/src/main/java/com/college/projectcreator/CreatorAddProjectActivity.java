package com.college.projectcreator;

import androidx.annotation.NonNull;
import androidx.appcompat.app.AppCompatActivity;

import android.app.DatePickerDialog;
import android.os.Bundle;
import android.view.MenuItem;
import android.view.View;
import android.widget.Button;
import android.widget.DatePicker;
import android.widget.EditText;
import android.widget.ProgressBar;
import android.widget.Toast;

import com.android.volley.AuthFailureError;
import com.android.volley.Request;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.StringRequest;
import com.college.mentor.AddProjectActivity;
import com.college.student.R;
import com.college.util.AppController;
import com.college.util.Keys;

import org.json.JSONException;
import org.json.JSONObject;

import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.HashMap;
import java.util.Locale;
import java.util.Map;

public class CreatorAddProjectActivity extends AppCompatActivity {

    EditText editText_name,editText_desc,editText_domain,editText_start,editText_end;
    Button button_add;
    Calendar myCalendar;
    ProgressBar progressBar;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_creator_add_project);
        getSupportActionBar().setTitle("Add Project");
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        editText_name=findViewById(R.id.edt_project_name);
        editText_desc=findViewById(R.id.edt_project_desc);
        editText_domain=findViewById(R.id.edt_project_domain);
        editText_start=findViewById(R.id.edt_project_start);
        editText_end=findViewById(R.id.edt_project_end);
        button_add=findViewById(R.id.btn_addProject);
        progressBar=findViewById(R.id.progressBar_add_project);
        myCalendar=Calendar.getInstance();
        editText_start.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                final DatePickerDialog.OnDateSetListener date = new DatePickerDialog.OnDateSetListener() {
                    @Override
                    public void onDateSet(DatePicker view, int year, int monthOfYear,
                                          int dayOfMonth) {
                        myCalendar.set(Calendar.YEAR, year);
                        myCalendar.set(Calendar.MONTH, monthOfYear);
                        myCalendar.set(Calendar.DAY_OF_MONTH, dayOfMonth);
                        updateLabel();

                    }

                };
                new DatePickerDialog(CreatorAddProjectActivity.this, date, myCalendar
                        .get(Calendar.YEAR), myCalendar.get(Calendar.MONTH),
                        myCalendar.get(Calendar.DAY_OF_MONTH)).show();
            }
        });

        editText_end.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                final DatePickerDialog.OnDateSetListener date = new DatePickerDialog.OnDateSetListener() {
                    @Override
                    public void onDateSet(DatePicker view, int year, int monthOfYear,
                                          int dayOfMonth) {
                        myCalendar.set(Calendar.YEAR, year);
                        myCalendar.set(Calendar.MONTH, monthOfYear);
                        myCalendar.set(Calendar.DAY_OF_MONTH, dayOfMonth);
                        updateLabel1();

                    }

                };
                new DatePickerDialog(CreatorAddProjectActivity.this, date, myCalendar
                        .get(Calendar.YEAR), myCalendar.get(Calendar.MONTH),
                        myCalendar.get(Calendar.DAY_OF_MONTH)).show();
            }
        });

        button_add.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                String p_name=editText_name.getText().toString().trim();
                String p_description=editText_desc.getText().toString().trim();
                String p_domain=editText_domain.getText().toString().trim();
                String p_start=editText_start.getText().toString().trim();
                String p_end=editText_end.getText().toString().trim();
                if (p_name.equals("") || p_description.equals("") || p_domain.equals("") || p_start.equals("") || p_end.equals("")){
                    Toast.makeText(CreatorAddProjectActivity.this, "Please fill all the details", Toast.LENGTH_SHORT).show();
                }else {
                    addProject(p_name,p_description,p_domain,p_start,p_end);
                }


            }
        });




    }

    private void updateLabel() {
        String myFormat = "yyyy-MM-dd"; //In which you need put here
        SimpleDateFormat sdf = new SimpleDateFormat(myFormat, Locale.US);
        editText_start.setText(sdf.format(myCalendar.getTime()));
    }
    private void updateLabel1(){
        String myFormat = "yyyy-MM-dd"; //In which you need put here
        SimpleDateFormat sdf = new SimpleDateFormat(myFormat, Locale.US);
        editText_end.setText(sdf.format(myCalendar.getTime()));
    }

    private void addProject(final String p_name, final String p_description, final String p_domain, final String p_start, final String p_end) {
        progressBar.setVisibility(View.VISIBLE);
        StringRequest request=new StringRequest(Request.Method.POST, Keys.URL.ADD_PROJECT, new Response.Listener<String>() {
            @Override
            public void onResponse(String response) {
                progressBar.setVisibility(View.GONE);
                try {
                    JSONObject jsonObject=new JSONObject(response);
                    if (jsonObject.getString("success").equals("1")){

                        Toast.makeText(CreatorAddProjectActivity.this, jsonObject.getString("message"), Toast.LENGTH_SHORT).show();
                        clear();
                        finish();

                    }else {
                        Toast.makeText(CreatorAddProjectActivity.this, jsonObject.getString("message"), Toast.LENGTH_SHORT).show();
                    }


                } catch (JSONException e) {
                    e.printStackTrace();
                    Toast.makeText(CreatorAddProjectActivity.this, "Technical problem arises", Toast.LENGTH_SHORT).show();
                }


            }
        }, new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError error) {
                progressBar.setVisibility(View.GONE);
                error.printStackTrace();
            }
        }){
            @Override
            protected Map<String, String> getParams() throws AuthFailureError {

                Map<String,String> params=new HashMap<>();
                params.put("p_name",p_name);
                params.put("p_description",p_description);
                params.put("p_domain",p_domain);
                params.put("p_start",p_start);
                params.put("p_end",p_end);
                return params;
            }
        };
        AppController.getInstance().add(request);


    }

    private void clear() {
        editText_name.setText("");
        editText_desc.setText("");
        editText_domain.setText("");
        editText_start.setText("");
        editText_end.setText("");



    }

    @Override
    public boolean onOptionsItemSelected(@NonNull MenuItem item) {
        switch (item.getItemId()){
            case android.R.id.home:

                onBackPressed();
                break;
        }

        return super.onOptionsItemSelected(item);

    }
}
