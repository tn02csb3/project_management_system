package com.college.pojo;

public class Task {
    private String pt_id;
    private String p_id;
    private String pt_title;
    private String pt_status;


    public Task(String pt_id, String p_id, String pt_title, String pt_status) {
        this.pt_id = pt_id;
        this.p_id = p_id;
        this.pt_title = pt_title;
        this.pt_status = pt_status;
    }


    public String getPt_id() {
        return pt_id;
    }

    public void setPt_id(String pt_id) {
        this.pt_id = pt_id;
    }

    public String getP_id() {
        return p_id;
    }

    public void setP_id(String p_id) {
        this.p_id = p_id;
    }

    public String getPt_title() {
        return pt_title;
    }

    public void setPt_title(String pt_title) {
        this.pt_title = pt_title;
    }

    public String getPt_status() {
        return pt_status;
    }

    public void setPt_status(String pt_status) {
        this.pt_status = pt_status;
    }
}
