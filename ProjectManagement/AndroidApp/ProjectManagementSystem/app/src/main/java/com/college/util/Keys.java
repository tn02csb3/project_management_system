package com.college.util;

public class Keys {

    public static String HOME_PATH = "http://127.0.0.1/cproject/projectmgmt/rahul/app/";

    public static String ADMIN = HOME_PATH + "admin/";
    public static String STUDENT = HOME_PATH + "student/";
    public static String GUIDE = HOME_PATH + "guide/";
    public static String MENTOR = HOME_PATH + "mentor/";
    public static String FACULTY = HOME_PATH + "coordinator/";
    public static String HOD = HOME_PATH + "hod/";
    public static String CREATOR = HOME_PATH + "creator/";
    public static String COMMON = HOME_PATH + "common/";


    public static class URL {
        public static String ADMIN_LOGIN = ADMIN + "admin_login.php";
        public static String STUDENT_LIST = ADMIN + "student_list.php";
        public static String SEE_ALL_PROJECTS = ADMIN + "see_all_projects.php";
        public static String SEE_ALL_GUIDES = ADMIN + "see_all_guides.php";
        public static String ASSIGN_PROJECT_GUIDE = ADMIN + "assign_project_guide.php";


        public static String STUDENT_REGISTER = STUDENT + "student_register.php";
        public static String STUDENT_LOGIN = STUDENT + "student_login.php";
        public static String GET_STUDENT_LIST = STUDENT + "get_student_list.php";
        public static String CREATE_TEAM = STUDENT + "create_team.php";
        public static String TASK_LIST = STUDENT + "see_project_task.php";
        public static String TEAM_LIST = STUDENT + "get_my_team.php";
        public static String SEE_INVITATION = STUDENT + "see_invitation.php";
        public static String ACCEPT_INVITATION = STUDENT + "accept_invitation.php";
        public static String SEND_INVITAION=STUDENT+"send_invitation.php";
        public static String AVAILABLE_PROJECTS=STUDENT+"see_available_project.php";
        public static String SELECT_PROJECT=STUDENT+"select_project.php";


        public static String GUIDE_LOGIN = GUIDE + "guide_login.php";
        public static String CHANGE_STATUS=GUIDE+"change_task_status.php";
        public static String GUIDE_PROJECTS=GUIDE+"get_guide_projects.php";
        public static String ADD_COMMENT = COMMON + "comment_project_task.php";


        public static String MENTOR_LOGIN = MENTOR + "mentor_login.php";
        public static String ADD_PROJECT = CREATOR + "add_project.php";
        public static String ADD_TASK = CREATOR + "add_project_task.php";


        public static String SEE_ALL_PROJECT = COMMON + "see_all_projects.php";


        public static String FACULTY_LOGIN = FACULTY + "coordinator_login.php";

        public static String HOD_LOGIN = HOD + "hod_login.php";

        public static String CREATOR_LOGIN = CREATOR + "creator_login.php";

        public static String COMMENT_LIST = COMMON + "get_project_task_comment.php";


    }
}
