package com.college.pojo;

public class Team {
    private String t_id;
    private String t_created;
    private String t_name;
    private String t_topic;
    private String t_time;


    public Team(String t_id, String t_created, String t_name, String t_topic, String t_time) {
        this.t_id = t_id;
        this.t_created = t_created;
        this.t_name = t_name;
        this.t_topic = t_topic;
        this.t_time = t_time;
    }

    public String getT_id() {
        return t_id;
    }

    public void setT_id(String t_id) {
        this.t_id = t_id;
    }

    public String getT_created() {
        return t_created;
    }

    public void setT_created(String t_created) {
        this.t_created = t_created;
    }

    public String getT_name() {
        return t_name;
    }

    public void setT_name(String t_name) {
        this.t_name = t_name;
    }

    public String getT_topic() {
        return t_topic;
    }

    public void setT_topic(String t_topic) {
        this.t_topic = t_topic;
    }

    public String getT_time() {
        return t_time;
    }

    public void setT_time(String t_time) {
        this.t_time = t_time;
    }
}
