package com.college.student;

import androidx.annotation.NonNull;
import androidx.appcompat.app.AppCompatActivity;

import android.content.Intent;
import android.os.Bundle;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ProgressBar;
import android.widget.Toast;

import com.android.volley.AuthFailureError;
import com.android.volley.Request;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.StringRequest;
import com.college.util.AppController;
import com.college.util.Keys;
import com.college.util.Loggers;
import com.college.util.SharedPreference;

import org.json.JSONException;
import org.json.JSONObject;

import java.util.HashMap;
import java.util.Map;

public class CreateTeamActivity extends AppCompatActivity {

    ProgressBar progressBar;
    EditText editText_name;
    Button button_create;
    String s_id,t_id;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_create_team);
        getSupportActionBar().setTitle("Create Team");
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        editText_name=findViewById(R.id.edt_team_name);
        button_create=findViewById(R.id.btn_create_team);
        progressBar=findViewById(R.id.progress_create_team);
        Intent i=getIntent();
        s_id=i.getStringExtra("s_id");



        button_create.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                String t_name=editText_name.getText().toString().trim();
                if (t_name.equals("")){
                    Toast.makeText(CreateTeamActivity.this, "Please fill all the details", Toast.LENGTH_SHORT).show();
                }else {
                    createTeam(s_id, t_name);
                }

            }
        });

    }

    private void createTeam(final String s_id, final String t_name) {
        progressBar.setVisibility(View.VISIBLE);
        StringRequest request=new StringRequest(Request.Method.POST, Keys.URL.CREATE_TEAM, new Response.Listener<String>() {
            @Override
            public void onResponse(String response) {
                progressBar.setVisibility(View.GONE);
                try {
                    JSONObject jsonObject=new JSONObject(response);
                    if (jsonObject.getString("success").equals("1")){
                        String t_id=jsonObject.getString("t_id");
                        Loggers.i(t_id);
                        Toast.makeText(CreateTeamActivity.this, jsonObject.getString("message"), Toast.LENGTH_SHORT).show();
                        Intent intent=new Intent(CreateTeamActivity.this,AvailableProjectActivity.class);
                        intent.putExtra("t_id",t_id);
                        startActivity(intent);
                        clear();


                    }else {
                        Toast.makeText(CreateTeamActivity.this, jsonObject.getString("message"), Toast.LENGTH_SHORT).show();
                    }


                } catch (JSONException e) {
                    e.printStackTrace();
                    Toast.makeText(CreateTeamActivity.this, "Technical problem arises", Toast.LENGTH_SHORT).show();
                }
            }
        }, new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError error) {
                progressBar.setVisibility(View.GONE);
                error.printStackTrace();
            }
        }){
            @Override
            protected Map<String, String> getParams() throws AuthFailureError {
                Map<String,String> params=new HashMap<>();
                params.put("s_id",s_id);
                params.put("t_name",t_name);
                return params;
            }
        };
        AppController.getInstance().add(request);


    }

    private void clear() {
        editText_name.setText("");

    }


//    @Override
//    public boolean onCreateOptionsMenu(Menu menu) {
//        MenuInflater inflater=getMenuInflater();
//        inflater.inflate(R.menu.menu,menu);
//        return true;
//    }

    @Override
    public boolean onOptionsItemSelected(@NonNull MenuItem item) {
        switch (item.getItemId()){
            case android.R.id.home:
                onBackPressed();
                break;
//            case R.id.action_logout:
//                if (SharedPreference.contains("s_id") && SharedPreference.contains("s_email") && SharedPreference.contains("s_password")){
//                    Intent intent=new Intent(CreateTeamActivity.this,LoginActivity.class);
//                    intent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
//                    SharedPreference.removeKey("s_email");
//                    SharedPreference.removeKey("s_password");
//                    SharedPreference.removeKey("s_id");
//                    startActivity(intent);
//                    finish();
//                    break;
//                }

        }

        return super.onOptionsItemSelected(item);
    }
}
