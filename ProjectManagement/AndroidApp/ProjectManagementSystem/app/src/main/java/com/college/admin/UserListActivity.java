package com.college.admin;

import androidx.annotation.NonNull;
import androidx.appcompat.app.AppCompatActivity;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import android.content.Intent;
import android.graphics.Color;
import android.os.Bundle;
import android.view.MenuItem;
import android.view.View;
import android.widget.Button;

import com.android.volley.AuthFailureError;
import com.android.volley.Request;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.StringRequest;
import com.college.adminadapter.UserAdapter;
import com.college.pojo.Student;
import com.college.student.R;
import com.college.util.AppController;
import com.college.util.Keys;
import com.college.util.Loggers;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.Map;

public class UserListActivity extends AppCompatActivity {

    RecyclerView recyclerView;
    ArrayList<Student> list;
    Button button_first,button_second,button_third;
    String value="0";


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_user_list);
        recyclerView=findViewById(R.id.recycler_user_list);
        button_first=findViewById(R.id.btn_f_year);
        button_second=findViewById(R.id.btn_s_year);
        button_third=findViewById(R.id.btn_t_year);
        list=new ArrayList<>();
        getSupportActionBar().setTitle("UserList");
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        LinearLayoutManager linearLayoutManager=new LinearLayoutManager(getApplicationContext());
        recyclerView.setLayoutManager(linearLayoutManager);
        recyclerView.setHasFixedSize(true);
        button_first.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                userList("2018");
                button_first.setBackgroundColor(getResources().getColor(R.color.color));
                button_second.setBackgroundColor(getResources().getColor(R.color.red));
                button_third.setBackgroundColor(getResources().getColor(R.color.red));



            }
        });
        button_second.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                userList("2019");
                button_first.setBackgroundColor(getResources().getColor(R.color.red));
                button_second.setBackgroundColor(getResources().getColor(R.color.color));
                button_third.setBackgroundColor(getResources().getColor(R.color.red));

            }
        });
        button_third.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                userList("2020");
                button_first.setBackgroundColor(getResources().getColor(R.color.red));
                button_second.setBackgroundColor(getResources().getColor(R.color.red));
                button_third.setBackgroundColor(getResources().getColor(R.color.color));
            }
        });





    }

    private void userList(final String s) {
        list.clear();
        StringRequest request=new StringRequest(Request.Method.POST, Keys.URL.STUDENT_LIST, new Response.Listener<String>() {
            @Override
            public void onResponse(String response) {
                Loggers.i(response);
                try {
                    JSONObject jsonObject=new JSONObject(response);
                    if (jsonObject.getString("success").equals("1")){
                        JSONArray jsonArray=jsonObject.getJSONArray("data");

                        for (int i=0;i<jsonArray.length();i++){
                            JSONObject jsonObject1=jsonArray.getJSONObject(i);
                            list.add(new Student(
                                    jsonObject1.getString("s_id"),
                                    jsonObject1.getString("s_name"),
                                    jsonObject1.getString("s_email"),
                                    jsonObject1.getString("s_phone"),
                                    jsonObject1.getString("s_c_id"),
                                    jsonObject1.getString("s_department"),
                                    jsonObject1.getString("s_year"),
                                    jsonObject1.getString("t_id"),s));


                        }
                        UserAdapter adapter=new UserAdapter(getApplicationContext(),list);
                        recyclerView.setAdapter(adapter);


                    }else {

                    }


                } catch (JSONException e) {
                    e.printStackTrace();
                }


            }
        }, new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError error) {

            }
        }){
            @Override
            protected Map<String, String> getParams() throws AuthFailureError {
                Map<String,String> params=new HashMap<>();
                return params;
            }
        };

        AppController.getInstance().add(request);





    }



    @Override
    public boolean onOptionsItemSelected(@NonNull MenuItem item) {
        switch (item.getItemId()){
            case android.R.id.home:

                onBackPressed();
                break;
        }

        return super.onOptionsItemSelected(item);
    }

    @Override
    public void onBackPressed() {
        Intent intent=new Intent(UserListActivity.this, AdminMenuActivity.class);
        startActivity(intent);
        finish();
    }





}
