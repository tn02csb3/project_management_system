package com.college.faculty;

import androidx.appcompat.app.AppCompatActivity;

import android.content.Intent;
import android.os.Bundle;
import android.util.Patterns;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ProgressBar;
import android.widget.Toast;

import com.android.volley.AuthFailureError;
import com.android.volley.Request;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.StringRequest;
import com.college.student.MenuActivity;
import com.college.student.R;
import com.college.util.AppController;
import com.college.util.Keys;
import com.college.util.SharedPreference;

import org.json.JSONException;
import org.json.JSONObject;

import java.util.HashMap;
import java.util.Map;

public class FacultyLoginActivity extends AppCompatActivity {


    Button button_login;
    EditText editText_email,editText_pass;
    ProgressBar progressBar;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_faculty_login);
        editText_email=findViewById(R.id.edt_f_email);
        editText_pass=findViewById(R.id.edt_f_pass);
        button_login=findViewById(R.id.btn_f_login);
        getSupportActionBar().setTitle("Login");
        progressBar=findViewById(R.id.progressBar_f_login);
        button_login.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                String c_email=editText_email.getText().toString().trim();
                String c_password=editText_pass.getText().toString().trim();
                if (c_email.equals("") || c_password.equals("")){
                    Toast.makeText(FacultyLoginActivity.this, "please fill all the details", Toast.LENGTH_SHORT).show();
                }else if (!Patterns.EMAIL_ADDRESS.matcher(c_email).matches()){
                    editText_email.setError("Invalid Email ID");
                }else {
                    login(c_email,c_password);
                }
            }
        });


    }

    private void login(final String c_email, final String c_password) {
        progressBar.setVisibility(View.VISIBLE);
        StringRequest request=new StringRequest(Request.Method.POST, Keys.URL.FACULTY_LOGIN, new Response.Listener<String>() {
            @Override
            public void onResponse(String response) {
                progressBar.setVisibility(View.GONE);
                try {
                    JSONObject jsonObject=new JSONObject(response);
                    if (jsonObject.getString("success").equals("1")){
                        JSONObject jsonObject1=jsonObject.getJSONObject("data");
                        String c_id=jsonObject1.getString("c_id");
                        Toast.makeText(FacultyLoginActivity.this, jsonObject.getString("message"), Toast.LENGTH_SHORT).show();
                        Intent intent=new Intent(FacultyLoginActivity.this, ProjectActivity.class);
                        intent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
                        SharedPreference.save("c_id",c_id);
                        SharedPreference.save("c_email",c_email);
                        SharedPreference.save("c_password",c_password);
                        startActivity(intent);
                        clear();
                        finish();

                    }else {
                        Toast.makeText(FacultyLoginActivity.this, jsonObject.getString("message"), Toast.LENGTH_SHORT).show();
                    }


                } catch (JSONException e) {
                    e.printStackTrace();

                }

            }
        }, new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError error) {
                progressBar.setVisibility(View.GONE);
                error.printStackTrace();
                Toast.makeText(FacultyLoginActivity.this, "Technical problem arises", Toast.LENGTH_SHORT).show();
            }
        }){
            @Override
            protected Map<String, String> getParams() throws AuthFailureError {
                Map<String,String> params=new HashMap<>();
                params.put("c_email",c_email);
                params.put("c_password",c_password);
                return params;
            }
        };
        AppController.getInstance().add(request);






    }

    private void clear() {

        editText_email.setText("");
        editText_pass.setText("");
    }


    @Override
    public void onBackPressed() {
        Intent intent=new Intent(FacultyLoginActivity.this, MenuActivity.class);
        intent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
        startActivity(intent);
        finish();
    }
}
