package com.college.projectcreator;

import androidx.annotation.NonNull;
import androidx.appcompat.app.AppCompatActivity;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import android.content.Intent;
import android.os.Bundle;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.view.View;
import android.widget.ProgressBar;
import android.widget.Toast;

import com.android.volley.AuthFailureError;
import com.android.volley.Request;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.StringRequest;
import com.college.commonadapter.CommentListAdapter;
import com.college.pojo.Comment;
import com.college.student.R;
import com.college.util.AppController;
import com.college.util.Keys;
import com.college.util.Loggers;
import com.college.util.SharedPreference;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.Map;

public class CommentActivity extends AppCompatActivity {

    RecyclerView recyclerView;
    ArrayList<Comment> list;
    ProgressBar progressBar;
    String pt_id;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_comment);
        recyclerView=findViewById(R.id.recycler_comment_list);
        list=new ArrayList<>();
        progressBar=findViewById(R.id.progress_comment);
        getSupportActionBar().setTitle("Comment List");
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        Intent i=getIntent();
        pt_id=i.getStringExtra("pt_id");
        LinearLayoutManager linearLayoutManager=new LinearLayoutManager(getApplicationContext());
        recyclerView.setLayoutManager(linearLayoutManager);
        recyclerView.setHasFixedSize(true);


    }

    @Override
    protected void onStart() {
        super.onStart();
        commentList(pt_id);

    }

    private void commentList(final String pt_id){
        progressBar.setVisibility(View.VISIBLE);
        StringRequest request=new StringRequest(Request.Method.POST, Keys.URL.COMMENT_LIST, new Response.Listener<String>() {
            @Override
            public void onResponse(String response) {
                Loggers.i(response);
                progressBar.setVisibility(View.GONE);
                try {
                    JSONObject jsonObject=new JSONObject(response);
                    if (jsonObject.getString("success").equals("1")){
                        JSONArray jsonArray=jsonObject.getJSONArray("data");

                        for (int i=0;i<jsonArray.length();i++){
                            JSONObject jsonObject1=jsonArray.getJSONObject(i);
                            list.add(new Comment(jsonObject1.getString("ptc_id"),
                                    jsonObject1.getString("pt_id"),
                                    jsonObject1.getString("pm_id"),
                                    jsonObject1.getString("g_id"),
                                    jsonObject1.getString("s_id"),
                                    jsonObject1.getString("c_id"),
                                    jsonObject1.getString("ptc_message"),
                                    jsonObject1.getString("ptc_time"),
                                    jsonObject1.getString("pm_name"),
                                    jsonObject1.getString("g_name"),
                                    jsonObject1.getString("s_name"),
                                    jsonObject1.getString("c_name")));

                        }
                        CommentListAdapter adapter=new CommentListAdapter(getApplicationContext(),list);
                        recyclerView.setAdapter(adapter);


                    }else {
                        Toast.makeText(CommentActivity.this, "No Data Available For Comment List", Toast.LENGTH_SHORT).show();
                    }


                } catch (JSONException e) {
                    e.printStackTrace();
                }


            }
        }, new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError error) {
                progressBar.setVisibility(View.GONE);
                error.printStackTrace();
            }
        }){
            @Override
            protected Map<String, String> getParams() throws AuthFailureError {
                Map<String,String> params=new HashMap<>();
                params.put("pt_id",pt_id);
                return params;

            }
        };

        AppController.getInstance().add(request);



    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        MenuInflater inflater=getMenuInflater();
        inflater.inflate(R.menu.menu,menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(@NonNull MenuItem item) {

        switch (item.getItemId()){
            case android.R.id.home:
                onBackPressed();
                break;


            case R.id.action_logout:
                if (SharedPreference.contains("pc_id") && SharedPreference.contains("pc_email") && SharedPreference.contains("pc_password")){
                    Intent i=new Intent(CommentActivity.this, ProjectCreatorLoginActivity.class);
                    i.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
                    SharedPreference.removeKey("pc_id");
                    SharedPreference.removeKey("pc_email");
                    SharedPreference.removeKey("pc_password");
                    startActivity(i);
                    finish();
                    break;
                }

                break;
        }

        return super.onOptionsItemSelected(item);
    }
}
