package com.college.student;

import androidx.annotation.NonNull;
import androidx.appcompat.app.AppCompatActivity;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import android.content.Intent;
import android.os.Bundle;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.view.View;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.ProgressBar;
import android.widget.TextView;
import android.widget.Toast;

import com.android.volley.AuthFailureError;
import com.android.volley.Request;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.StringRequest;
import com.college.studentadapter.ShowInvitationAdapter;
import com.college.pojo.ShowInvitation;
import com.college.util.AppController;
import com.college.util.Keys;
import com.college.util.Loggers;
import com.college.util.SharedPreference;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.Map;

public class TeamActivity extends AppCompatActivity {

    String s_id,t_id,t_name,p_id,p_name;
    RecyclerView recyclerView_invite;
    ArrayList<ShowInvitation> invitations;
    ProgressBar progressBar;
    Button button_invitation,button_send;
    TextView tv_name,tv_p_name;
    ImageView imageView;
    LinearLayout linearLayout;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_team);
        AppController.initialize(getApplicationContext());
        linearLayout=findViewById(R.id.l1);
        imageView=findViewById(R.id.img_list);
        tv_name=findViewById(R.id.txt_t_name);
        tv_p_name=findViewById(R.id.txt_p_name);
        progressBar=findViewById(R.id.progressbar_team);
        button_invitation=findViewById(R.id.btn_show_invitation);
        recyclerView_invite=findViewById(R.id.recycler_invitaion_list);
        button_send=findViewById(R.id.btn_send_invitation);
        linearLayout=findViewById(R.id.l1);
        linearLayout.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent=new Intent(TeamActivity.this, TabActivity.class);
                intent.putExtra("p_id",p_id);
                startActivity(intent);

            }
        });
        button_invitation.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                showInvitation(s_id);
            }
        });



        s_id= SharedPreference.get("s_id");
        invitations=new ArrayList<>();
        getSupportActionBar().setTitle("Team List");
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        LinearLayoutManager layoutManager=new LinearLayoutManager(getApplicationContext());
        recyclerView_invite.setLayoutManager(layoutManager);
        recyclerView_invite.setHasFixedSize(true);
        button_send.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent=new Intent(TeamActivity.this,InvitationActivity.class);
                intent.putExtra("t_id",t_id);
                intent.putExtra("t_name",t_name);
                startActivity(intent);
            }
        });


    }

    private void showInvitation(final String s_id) {
        invitations.clear();
        progressBar.setVisibility(View.VISIBLE);
        StringRequest request=new StringRequest(Request.Method.POST, Keys.URL.SEE_INVITATION, new Response.Listener<String>() {
            @Override
            public void onResponse(String response) {
                Loggers.i(response);
                progressBar.setVisibility(View.GONE);
                try {
                    JSONObject jsonObject=new JSONObject(response);
                    if (jsonObject.getString("success").equals("1")){
                        JSONArray jsonArray=jsonObject.getJSONArray("data");

                        for (int i=0;i<jsonArray.length();i++){
                            JSONObject jsonObject1=jsonArray.getJSONObject(i);
                            invitations.add(new ShowInvitation(jsonObject1.getString("in_id"),
                                    jsonObject1.getString("t_id"),
                                    jsonObject1.getString("in_from_id"),
                                    jsonObject1.getString("in_to_id"),
                                    jsonObject1.getString("in_status"),
                                    jsonObject1.getString("in_time"),
                                    jsonObject1.getString("in_from_name"),
                                    jsonObject1.getString("in_to_name"),
                                    jsonObject1.getString("t_name")));


                        }
                        ShowInvitationAdapter adapter=new ShowInvitationAdapter(getApplicationContext(),invitations);
                        recyclerView_invite.setAdapter(adapter);

                    }else {
                        Toast.makeText(TeamActivity.this, "No Data Available For Invitation", Toast.LENGTH_SHORT).show();
                    }


                } catch (JSONException e) {
                    e.printStackTrace();
                }


            }
        }, new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError error) {
                progressBar.setVisibility(View.GONE);
                error.printStackTrace();
            }
        }){
            @Override
            protected Map<String, String> getParams() throws AuthFailureError {
                Map<String,String> params=new HashMap<>();
                params.put("s_id",s_id);
                return params;
            }
        };

        AppController.getInstance().add(request);



    }

    @Override
    protected void onStart() {
        super.onStart();
        showTeamList(s_id);
    }

    private void showTeamList(final String s_id) {

        progressBar.setVisibility(View.VISIBLE);
        StringRequest request=new StringRequest(Request.Method.POST, Keys.URL.TEAM_LIST, new Response.Listener<String>() {
            @Override
            public void onResponse(String response) {
                Loggers.i(response);
                progressBar.setVisibility(View.GONE);
                try {
                    JSONObject jsonObject=new JSONObject(response);
                    if (jsonObject.getString("success").equals("1")){
                        JSONObject jsonObject1=jsonObject.getJSONObject("data");
                        JSONObject jsonObject2=jsonObject.getJSONObject("data2");
                        t_id=jsonObject1.getString("t_id");
                        String t_created=jsonObject1.getString("t_created");
                        t_name=jsonObject1.getString("t_name");
                        String t_topic=jsonObject1.getString("t_topic");
                        String t_time= jsonObject1.getString("t_time");
                        p_id=jsonObject2.getString("p_id");
                        p_name=jsonObject2.getString("p_name");
                        tv_name.setText("Team Name: " + t_name);
                        tv_p_name.setText("Project Name: " +p_name);
                        button_send.setVisibility(View.VISIBLE);
                        imageView.setVisibility(View.VISIBLE);



                    }else {

                        tv_name.setText("No Data Available For Team");
                        tv_p_name.setText("No Data Available For Project");
                    }


                } catch (JSONException e) {
                    e.printStackTrace();
                }


            }
        }, new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError error) {
                progressBar.setVisibility(View.GONE);
                error.printStackTrace();
            }
        }){
            @Override
            protected Map<String, String> getParams() throws AuthFailureError {
                Map<String,String> params=new HashMap<>();
                params.put("s_id",s_id);
                return params;

            }
        };

        AppController.getInstance().add(request);


    }


    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        MenuInflater inflater=getMenuInflater();
        inflater.inflate(R.menu.add_project,menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(@NonNull MenuItem item) {

        switch (item.getItemId()){
            case R.id.add_project:
                Intent i=new Intent(TeamActivity.this,CreateTeamActivity.class);
                i.putExtra("s_id",s_id);
                startActivity(i);
                break;
            case android.R.id.home:

                onBackPressed();
                break;
        }

        return super.onOptionsItemSelected(item);
    }


    @Override
    public void onBackPressed() {
        Intent intent=new Intent(TeamActivity.this,MenuActivity.class);
        intent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
        startActivity(intent);
        finish();
    }
}
