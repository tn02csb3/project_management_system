package com.college.mentor;

import androidx.appcompat.app.AppCompatActivity;

import android.content.Intent;
import android.os.Bundle;
import android.util.Patterns;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ProgressBar;
import android.widget.Toast;

import com.android.volley.AuthFailureError;
import com.android.volley.Request;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.StringRequest;
import com.college.student.MenuActivity;
import com.college.student.R;
import com.college.util.AppController;
import com.college.util.Keys;
import com.college.util.SharedPreference;

import org.json.JSONException;
import org.json.JSONObject;

import java.util.HashMap;
import java.util.Map;

public class MentorLoginActivity extends AppCompatActivity {



    Button button_login;
    EditText editText_email,editText_pass;
    ProgressBar progressBar;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_mentor_login);
        editText_email=findViewById(R.id.edt_m_email);
        editText_pass=findViewById(R.id.edt_m_pass);
        button_login=findViewById(R.id.btn_m_login);
        progressBar=findViewById(R.id.progressBar_m_login);
        getSupportActionBar().setTitle("Login");
        button_login.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                String pm_email=editText_email.getText().toString().trim();
                String pm_password=editText_pass.getText().toString().trim();
                if (pm_email.equals("") || pm_password.equals("")){
                    Toast.makeText(MentorLoginActivity.this, "please fill all the details", Toast.LENGTH_SHORT).show();
                }else if (!Patterns.EMAIL_ADDRESS.matcher(pm_email).matches()){
                    editText_email.setError("Invalid Email ID");
                }else {
                    login(pm_email,pm_password);
                }
            }
        });


    }

    private void login(final String pm_email, final String pm_password) {
        progressBar.setVisibility(View.VISIBLE);
        StringRequest request=new StringRequest(Request.Method.POST, Keys.URL.MENTOR_LOGIN, new Response.Listener<String>() {
            @Override
            public void onResponse(String response) {
                progressBar.setVisibility(View.GONE);
                try {
                    JSONObject jsonObject=new JSONObject(response);
                    if (jsonObject.getString("success").equals("1")){

                        JSONObject jsonObject1=jsonObject.getJSONObject("data");
                        String pm_id=jsonObject1.getString("pm_id");
                        Toast.makeText(MentorLoginActivity.this, jsonObject.getString("message"), Toast.LENGTH_SHORT).show();
                        Intent intent=new Intent(MentorLoginActivity.this, MentorProjectListActivity.class);
                        SharedPreference.save("pm_email",pm_email);
                        SharedPreference.save("pm_password",pm_password);
                        SharedPreference.save("pm_id",pm_id);
                        startActivity(intent);
                        clear();
                        finish();

                    }else {
                        Toast.makeText(MentorLoginActivity.this, jsonObject.getString("message"), Toast.LENGTH_SHORT).show();
                    }


                } catch (JSONException e) {
                    e.printStackTrace();
                    Toast.makeText(MentorLoginActivity.this, "Technical problem arises", Toast.LENGTH_SHORT).show();
                }

            }
        }, new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError error) {
                progressBar.setVisibility(View.GONE);
                error.printStackTrace();
            }
        }){
            @Override
            protected Map<String, String> getParams() throws AuthFailureError {
                Map<String,String> params=new HashMap<>();
                params.put("pm_email",pm_email);
                params.put("pm_password",pm_password);
                return params;
            }
        };

        AppController.getInstance().add(request);





    }

    private void clear() {

        editText_email.setText("");
        editText_pass.setText("");
    }


    @Override
    public void onBackPressed() {
        Intent intent=new Intent(MentorLoginActivity.this, MenuActivity.class);
        intent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
        startActivity(intent);
        finish();

    }
}
